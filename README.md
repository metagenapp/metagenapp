# MetagenApp

A platform agnostic metagenomics pipeline for analysing  Whole Genome Sequencing (WGS) and 16S NGS data (paired end). 
Required tools and databases are provided as Docker containers. The App can be hosted or run on a local machine. 
It provides a browser based user interface which can be used for running the pipeline and viewing results.


## Dependencies

- Docker
- Python >= 3.6
- pipenv

## Running the App

Start the App with the following command:

~~~bash
pipenv run python3 ./MetagenApp.py 
~~~

Start a pipeline run on the UI:



Input data files are stored in the directory *metagenapp/web/django_project/input*.
If the pipline is running the first time, reference databases will be downloaded and stored in the following directory:
*metagenapp/MetagenApp/data*.

## App configuration:

Default config values can be changed by starting the app with a yaml formated config file. 

~~~bash
python3 ./MetagenApp.py -c config.yaml
~~~ 

~~~yaml
# custom databases:

kraken2_db: "/path/to/kraken2_db"
kmer_len : 35 # kmer-length the kraken2 database has been built with (default 35)
metaphlan2_db: /path/to/mpa_v20_m200 # required for humann2
humann2_chocophlan: /path/to/chocophlan
humann2_uniref: /path/to/uniref
silva_qiime: /path/to/silva_qiime


## pipeline run:

input_directory: "/home/josmos/pycharmprojects/metagenapp/test" # directory with input fastq files

max_cores: 4 # maximum number of cpu cores to use
mode: "WGS" # "WGS" or "16S"
analysis: "experiment1"

# trimming parameters: (Trimmomatic)

trim_leading: 25 # [0-30] (0: No leading)
trim_trailing: 25 # [0-30] (0: No trailing)
trim_minlength: 70 # [50-1000]
trim_sliding_window: 4 # [1-10]
trim_sliding_qual: 18 # [10-60]
trim_illumina_clip: "TruSeq3-PE-2" # ["No Clipping", "TruSeq2-PE", "TruSeq3-PE-2" "TruSeq3-PE", "NexteraPE-PE"]
# Illumina clipping parameters:
trim_seed_mismatches: 2 # [2-100]
trim_palindrome_clip_theshold: 10 # [10-100]
trim_simple_clip_theshold: 10 # [10-100]
trim_minAdapterLength: 0 # 0 for no min Adapter Length

# Sample meta data:
parameters:
- parameter: status
  datatype: categorical
  values: [desease, control] # possible options
  
- parameter: dna # e.g. input dna in µg/ml
- datatype: metric
- values []

samples:
    - name: Sample1
      files: [test_1_fwd.fastq.gz,
              test_1_rev.fastq.gz]
      status: desease # define value for parameter
      dna: 600
      
    - name: Sample2
      files: [test_2_fwd.fastq.gz,
              test_2_rev.fastq.gz]
      status: control
      dna: 550
~~~

To prevent the download of reference databases you can set an altenative path for each database.
Make sure the directories contain all required files.
The WGS pipeline will use the minikraken database as default. Given the required hardware is availiable, it is recommended to use a larger database.

The pipeline can be started from the command line by setting the config arguments listed under *pipeline run*.
This also prevents copying the files to the input directiory of the project folder. Result files are stored in the directory of the input data.

## The Pipeline

![](img/workflow-1.png)

## Troubleshooting

#### Error: Filedescriptor out of range in select():

Edit the python docker library: 

change line 31 in *~/.local/share/virtualenvs/metagenapp--oqpMI2d/lib/python3.7/site-packages/docker/utils/socket.py*

from:

        select.select([socket], [], [])
to:
 
        select.poll()
        