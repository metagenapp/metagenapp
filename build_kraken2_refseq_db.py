#!/usr/bin/env python3.6

"""
Script for building a kraken2 database with ncbi refseq
includes refseqs all refseqs provided by kraken-download
additionally the groups invertebrate, vertebrate_mammalian and vertebrate_other

dependencies:

    pip install ncbi-genome-download

"""

import gzip
import multiprocessing as mp
import shutil
from datetime import date
from pathlib import Path
from subprocess import call

import psutil

kmer_len = 41
threads = 60
db_name = f"kraken2_all_refseqs_2019-05-14"
cwd = Path("/group/opt/databases/kraken2/")
db_dir = cwd.joinpath(db_name)
db_dir.mkdir(exist_ok=True)


def execute(cmd, cwd=cwd):
    call(cmd, shell=True, cwd=str(cwd))


print("Start donwnloading Taxonomy")

execute(f"kraken2-build --download-taxonomy --db {db_name}")

print("Start downloading Librarys")


def exec_dllib(group):
    t = int(mp.cpu_count() / 8) if mp.cpu_count() > 16 else 1
    execute(f"kraken2-build --download-library {group} --threads {t} --db {db_name}")


n_cores = mp.cpu_count() if mp.cpu_count() < 8 else 8
p = mp.Pool(n_cores)
p.map(exec_dllib, ["archaea", "bacteria", "plasmid", "viral", "human", "fungi", "plant", "protozoa"])
p.close()
p.join()

[execute(f"ncbi-genome-download -v --refseq-category all --format fasta {group}", cwd=db_dir) for group in
 ['invertebrate', 'vertebrate_mammalian', 'vertebrate_other']]


def add_lib(path):
    unzipped = str(path.with_suffix(""))
    print(f"add file to library: {unzipped}")
    with gzip.open(path, "rb") as f_in:
        with open(unzipped, "wb") as f_out:
            shutil.copyfileobj(f_in, f_out)
    path.unlink()
    execute(f"kraken2-build --add-to-library {unzipped} --threads {threads} --db {db_dir}")


paths = [p for p in db_dir.joinpath("refseq").glob('**/*_genomic.fna.gz')]
p = mp.Pool(n_cores)
p.map(add_lib, paths)
p.close()
p.join()

print("Start building Database")

mem = int(psutil.virtual_memory().available * 0.9)
execute(f"kraken2-build --build --threads {threads} --kmer-len {kmer_len} --db {db_name} --max-db-size {mem}")