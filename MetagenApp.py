##! /usr/bin/env python3

import argparse

from MetagenApp.network import start_network
from MetagenApp.images import WebImage, WebImageDev, WebImageDevNginx, DbImage, WebImageNginx
from MetagenApp.worker import Worker
import MetagenApp.config as config
import time
import traceback
from pathlib import Path
import docker
from psycopg2 import DatabaseError, OperationalError


parser = argparse.ArgumentParser()
parser.add_argument("-c", "--configfile",
                    type=argparse.FileType("r"),
                    metavar="CONFIGFILE",
                    help="Passing the configfile directly starts a pipeline run")

parser.add_argument("-n", "--notebook", action="store_true", help="Run Jupyter Notebook mode (only in dev mode)")
parser.add_argument("-d", "--dev", action="store_true", help="Run with django development server")

args = parser.parse_args()

if args.configfile:
    config.from_configfile(args.configfile)

config.input_directory.mkdir(exist_ok=True)
config.postgres_volume_directory.mkdir(exist_ok=True)
django_input = Path.cwd() / "web/django/input"
django_static = Path.cwd() / "web/django/static"
django_input.mkdir(exist_ok=True)
django_static.mkdir(exist_ok=True)
dockerclient = docker.from_env(timeout=9999999)
config.set("dockerclient", dockerclient)
metagen_net = start_network(dockerclient)
try:
    if args.dev:
        config.set("container_mount_dir", "/code/")
        db_image = DbImage(dockerclient, "metagen_db")
        db_image.run()
        if args.notebook:
            web_image = WebImageDev(dockerclient, map_uid=False)
            ngnix_image_dev = WebImageDevNginx(dockerclient)
            web_container = web_image.run_notebook()
            ngnix_container = ngnix_image_dev.runserver()
        else:
            web_image = WebImageDev(dockerclient, map_uid=True)
            ngnix_container = None
            web_container = web_image.runserver()

    else:
        db_image = DbImage(dockerclient, "metagen_db")
        db_image.run()
        web_image = WebImage(dockerclient)
        ngnix_image = WebImageNginx(dockerclient)
        web_container = web_image.runserver()
        ngnix_container = ngnix_image.runserver()

except Exception as e:
    print(e)
    metagen_net.containers.remove(force=True)
    metagen_net.remove()
    exit()


worker = Worker(dockerclient, db_image, web_container, ngnix_container, metagen_net)

try:
    if config.analysis:
        database_connected = False
        while not database_connected:
            try:
                config.db_connector.set_ip()
                database_connected = True

            except OperationalError:
                time.sleep(1)

        web_container.add_run_and_sample()
        migrated = True

    worker.work()


except KeyboardInterrupt:
    exit()

except Exception as e:
    traceback.print_tb(e.__traceback__)
    print(e)
    exit()

finally:
    worker.stop_work()

