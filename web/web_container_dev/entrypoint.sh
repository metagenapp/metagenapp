#!/bin/sh

python manage.py wait_for_db
python manage.py makemigrations --no-input
python manage.py migrate || { echo 'migrate failed' ; exit 1; }

exec "$@"
