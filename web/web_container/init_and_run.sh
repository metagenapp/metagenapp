#!/usr/bin/env bash

python manage.py wait_for_db
python manage.py makemigrations --no-input
python manage.py migrate || { echo 'migrate failed' ; exit 1; }
python manage.py collectstatic --no-input
#python manage.py initadmin
#python manage.py initial_data

/usr/local/bin/uwsgi --ini /etc/uwsgi/apps-enabled/uwsgi-app.ini
