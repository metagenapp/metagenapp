from django.shortcuts import render
from django.views.decorators.clickjacking import xframe_options_exempt

from app.session_controller import SessionController, update_session
from .forms import SampleMenu, SampleMenuRadio, RankMenu, StrandMenu
from .models import PipelineRun, Sample, KronaPlot, EmperorPlot, PipelineQC, SampleParameterPreset


@update_session
def results_run(request, run_id):
    template_name = "results/results.html"
    runs = PipelineRun.objects.all()

    if request.method == 'GET':
        return render(request, template_name, {
            "runs": runs,
        })


@update_session
def results_qc(request, run_id):
    template_name = "results/qc.html"
    samples = Sample.objects.filter(run_id=run_id)
    session = SessionController(request)
    qcs = None
    strand = None
    strand_trim = None

    if request.method == "POST":
        samples_form = SampleMenuRadio(request.POST, qs=samples, prefix="samples_form")
        strand_form = StrandMenu(request.POST, prefix="strand_form")
        if samples_form.is_valid() and strand_form.is_valid():
            qcs = PipelineQC.objects.filter(sample_id=session.active_samples[0], run_id=run_id)
            strand = strand_form.cleaned_data["strand"]
            strand_trim = strand + "_trim"

        else:
            samples_form = SampleMenuRadio(qs=samples,
                                           prefix="samples_form")
    else:
        try:
            initial = {"samples_form-samples": session.active_samples[0]}
            qcs = PipelineQC.objects.filter(sample_id=session.active_samples[0], run_id=run_id)
        except IndexError:
            initial = {}
        samples_form = SampleMenuRadio(initial, qs=samples, prefix="samples_form")
        strand_form = StrandMenu(prefix="strand_form", initial={"strand": "fastqc_fwd"})

    return render(request, template_name, {
        "runs": PipelineRun.objects.all(),
        "samples_form": samples_form,
        "strand_form": strand_form,
        "qcs": qcs,
        "strand": strand,
        "strand_trim": strand_trim
    })


@xframe_options_exempt
def results_iframe_qc(request, sample_id, strand):
    template_name = "results/qc_iframe.html"
    fastqc = PipelineQC.objects.get(sample_id=sample_id)
    content = fastqc.fastqc_fwd if strand == "fastqc_fwd" else fastqc.fastqc_rev
    return render(request, template_name, {"content": content})


@xframe_options_exempt
def results_iframe_qc_trim(request, sample_id, strand):
    template_name = "results/qc_iframe_trim.html"
    fastqc = PipelineQC.objects.get(sample_id=sample_id)
    content = fastqc.fastqc_fwd_trim if strand == "fastqc_fwd_trim" else fastqc.fastqc_rev_trim
    return render(request, template_name, {"content": content})


@update_session
def results_taxonomy(request, run_id):
    template_name = "results/taxonomy.html"
    samples = Sample.objects.filter(run_id=run_id)
    session = SessionController(request)
    kronaplots = KronaPlot.objects.filter(sample_id__in=session.active_samples, run_id=run_id)

    if request.method == "POST":
        samples_form = SampleMenu(request.POST, qs=samples, prefix="samples_form", session=session)

    else:
        samples_form = SampleMenu(qs=samples, prefix="samples_form", session=session)

    return render(request, template_name, {
        "runs": PipelineRun.objects.all(),
        "samples_form": samples_form,
        "kronaplots": kronaplots,
    })


@xframe_options_exempt
def iframe_krona(request, plot_id):
    template_name = "results/krona_iframe.html"
    plot = KronaPlot.objects.get(pk=plot_id)
    content = plot.kronahtml
    return render(request, template_name, {"content": content})


@xframe_options_exempt
def pcoa_iframe_en(request, plot_id):
    template_name = "results/pcoa_iframe.html"
    plot = EmperorPlot.objects.get(pk=plot_id)
    emperordata = plot.emperordata_en
    print(emperordata)

    return render(request, template_name, {"emperordata": emperordata})


@xframe_options_exempt
def pcoa_iframe_de(request, plot_id):
    template_name = "results/pcoa_iframe.html"
    plot = EmperorPlot.objects.get(pk=plot_id)
    emperordata = plot.emperordata_de

    return render(request, template_name, {"emperordata": emperordata})

@update_session
def results_pcoa(request, run_id):
    template_name = "results/pcoa.html"
    runs = PipelineRun.objects.all()
    rank = "S"

    if request.method == "POST":
        rankform = RankMenu(request.POST)
        if rankform.is_valid():
            rank = rankform.cleaned_data["rank"]

    else:
        rankform = RankMenu(initial={"rank": rank})

    emperorplot = EmperorPlot.objects.get(run_id=run_id, rank=rank)

    return render(request, template_name, {
        "runs": runs,
        "rankform": rankform,
        "plot": emperorplot,
    })
