from django import forms
from .models import Sample
from django.utils.translation import gettext_lazy as _


class StrandMenu(forms.Form):
    def __init__(self, *args, **kwargs):
        super(StrandMenu, self).__init__(*args, **kwargs)
        self.initial['strand'] = 'fastqc_fwd'

    choices = [("fastqc_fwd", _("Forward")),
               ("fastqc_rev", _("Reverse"))]
    strand = forms.ChoiceField(label=_("strand"),
                               widget=forms.RadioSelect(attrs={"class": "radio-form"}),
                               choices=choices,
                               required=True)


class SampleMenu(forms.Form):
    class Meta:
        model = Sample
        fields = ('name',)

    def __init__(self, *args, qs=None, session=None, **kwargs):
        super(SampleMenu, self).__init__(*args, **kwargs)
        initial = session.active_samples if session else []
        if qs:
            qs = [[x.id, x.name] for x in reversed(qs)]
            self.fields['samples'] = forms.MultipleChoiceField(choices=qs,
                                                               widget=forms.CheckboxSelectMultiple(),
                                                               required=False,
                                                               label="",
                                                               initial=initial)


class SampleMenuRadio(forms.Form):
    class Meta:
        model = Sample
        fields = ('name',)

    def __init__(self, *args,  qs=None, **kwargs):
        super(SampleMenuRadio, self).__init__(*args, **kwargs)
        if qs:
            qs = [(str(x.id), x.name) for x in reversed(qs)]
            self.fields['samples'] = forms.ChoiceField(choices=qs,
                                                       widget=forms.RadioSelect(attrs={"class": "radio-form"}),
                                                       required=True,
                                                       label="")


class RankMenu(forms.Form):
    def __init__(self, *args, **kwargs):
        super(RankMenu, self).__init__(*args, **kwargs)
        self.initial['rank'] = 'S'

    choices = [("S", _("Species")),
               ("G", _("Genus")),
               ("F", _("Family")),
               ("O", _("Order")),
               ("C", _("Class")),
               ("P", _("Phylum"))]
    rank = forms.ChoiceField(label="",
                               widget=forms.RadioSelect(attrs={"class": "radio-form"}),
                               choices=choices,
                               required=True)
