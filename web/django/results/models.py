import os
import uuid

from django.db import models
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from mptt.models import MPTTModel, TreeForeignKey


class PipelineRun(models.Model):
    id = models.UUIDField(primary_key=True,
                          default=uuid.uuid4,
                          editable=False)
    analysis_en = models.CharField(_("english name"),
                                   max_length=20,
                                   unique=True)
    analysis_de = models.CharField(_("german name"),
                                   max_length=20,
                                   blank=True)
    mode = models.CharField(_("mode"),
                            choices=[(x, x) for x in ["WGS", "16S"]],
                            default="16S",
                            max_length=3)
    cpu_cores = models.IntegerField(_("CPU cores"),
                                    choices=[(x, x) for x in range(1, os.cpu_count() + 1)],
                                    default=os.cpu_count() - 1)
    analysis_date = models.DateTimeField(_("date"), default=timezone.now)
    status = models.CharField(max_length=10, default="new")
    description_en = models.TextField(_("description (en)"), max_length=500, blank=True)
    description_de = models.TextField(_("description (ge)"), max_length=500, blank=True)
    details_en = models.TextField(_("detailed description (en)"), max_length=10000, blank=True)
    details_de = models.TextField(_("detailed description (ge)"), max_length=10000, blank=True)
    image = models.ImageField(_("image"), blank=True)

    def __str__(self):
        return self.analysis_en


def set_upload_path(instance, filename):
    return f"input/{instance.run.analysis_en}/{filename}"


class Sample(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    run = models.ForeignKey(PipelineRun, on_delete=models.CASCADE)
    name = models.CharField(_("Sample Name"), max_length=20)
    sample_file_fwd = models.FileField(_("forward file"), upload_to=set_upload_path)
    sample_file_rev = models.FileField(_("reverse file"), upload_to=set_upload_path)


class KronaPlot(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    run_id = models.ForeignKey(PipelineRun, on_delete=models.CASCADE)
    sample_id = models.ForeignKey(Sample, on_delete=models.CASCADE)
    kronahtml = models.TextField("kronahtml")


class EmperorPlot(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    run_id = models.ForeignKey(PipelineRun, on_delete=models.CASCADE)
    rank = models.CharField("rank", max_length=1)
    emperordata_en = models.TextField("emperordata")
    emperordata_de =  models.TextField("emperordata", blank=True, null=True)


class Taxon(MPTTModel):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    parent = TreeForeignKey("self", on_delete=models.CASCADE, null=True, blank=True,
                            related_name="children")
    sample_id = models.ForeignKey(Sample, on_delete=models.CASCADE)
    run_id = models.ForeignKey(PipelineRun, on_delete=models.CASCADE)
    percentage = models.FloatField()
    fragments_root = models.IntegerField()
    fragments = models.IntegerField()
    abundance = models.IntegerField()
    rank = models.CharField(max_length=4)
    tax_id = models.CharField(max_length=30, blank=True, null=True)
    name = models.CharField(max_length=200)

    class MPTTMeta:
        order_insertion_by = ['name']


class SampleParameterPreset(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    run = models.ForeignKey(PipelineRun, on_delete=models.CASCADE)
    parameter_en = models.CharField("parameter (english)", max_length=20)
    parameter_de = models.CharField("parameter (german)", max_length=20)
    datatype = models.CharField(_("data type"), max_length=11)
    value_1 = models.CharField(_("value 1"), max_length=20, blank=True, null=True)
    value_2 = models.CharField(_("value 2"), max_length=20, blank=True, null=True)
    value_3 = models.CharField(_("value 3"), max_length=20, blank=True, null=True)
    value_4 = models.CharField(_("value 4"), max_length=20, blank=True, null=True)
    value_5 = models.CharField(_("value 5"), max_length=20, blank=True, null=True)
    value_6 = models.CharField(_("value 6"), max_length=20, blank=True, null=True)
    value_7 = models.CharField(_("value 7"), max_length=20, blank=True, null=True)
    value_8 = models.CharField(_("value 8"), max_length=20, blank=True, null=True)
    value_9 = models.CharField(_("value 9"), max_length=20, blank=True, null=True)
    value_10 = models.CharField(_("value 10"), max_length=20, blank=True, null=True)
    value_1_de = models.CharField(_("value 1 (ge)"), max_length=20, blank=True, null=True)
    value_2_de = models.CharField(_("value 2 (ge)"), max_length=20, blank=True, null=True)
    value_3_de = models.CharField(_("value 3 (ge)"), max_length=20, blank=True, null=True)
    value_4_de = models.CharField(_("value 4 (ge)"), max_length=20, blank=True, null=True)
    value_5_de = models.CharField(_("value 5 (ge)"), max_length=20, blank=True, null=True)
    value_6_de = models.CharField(_("value 6 (ge)"), max_length=20, blank=True, null=True)
    value_7_de = models.CharField(_("value 7 (ge)"), max_length=20, blank=True, null=True)
    value_8_de = models.CharField(_("value 8 (ge)"), max_length=20, blank=True, null=True)
    value_9_de = models.CharField(_("value 9 (ge)"), max_length=20, blank=True, null=True)
    value_10_de = models.CharField(_("value 10 (ge)"), max_length=20, blank=True, null=True)


class SampleParameter(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    sample = models.ForeignKey(Sample, on_delete=models.CASCADE)
    run = models.ForeignKey(PipelineRun, on_delete=models.CASCADE)
    name_en = models.CharField(_("parameter (english)"), max_length=20)
    name_de = models.CharField(_("parameter (german)"), max_length=20)
    value = models.CharField(_("value"), max_length=20)
    value_de = models.CharField(_("value (german)"), max_length=20)
    datatype = models.CharField(_("type"), max_length=11)
    order = models.IntegerField(null=True)


class PipelineQC(models.Model):
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    run_id = models.ForeignKey(PipelineRun, on_delete=models.CASCADE)
    sample_id = models.ForeignKey(Sample, on_delete=models.CASCADE)
    fastqc_fwd = models.TextField("fastqc_fwd")
    fastqc_rev = models.TextField("fastqc_rev")
    fastqc_fwd_trim = models.TextField("fastqc_fwd_trim", default="")
    fastqc_rev_trim = models.TextField("fastqc_rev_trim", default="")
