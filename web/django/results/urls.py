from django.urls import path

from . import views

app_name = "results"
urlpatterns = [
    path("<uuid:run_id>/qc/", views.results_qc, name="qc"),
    path("<uuid:run_id>/", views.results_run, name="run"),
    path("<uuid:run_id>/taxonomy/", views.results_taxonomy, name="taxonomy"),
    path("<uuid:run_id>/pcoa/", views.results_pcoa, name="pcoa"),
    path("pcoa_iframe/<uuid:plot_id>/en", views.pcoa_iframe_en, name="pcoa_iframe_en"),
    path("pcoa_iframe/<uuid:plot_id>/de", views.pcoa_iframe_de, name="pcoa_iframe_de"),
    path("kronaplot/<uuid:plot_id>", views.iframe_krona, name="krona_iframe"),
    path("qc/fastqc/<str:strand>/<uuid:sample_id>/", views.results_iframe_qc, name="qc_iframe"),
    path("qc/trim_fastqc/<str:strand>/<uuid:sample_id>", views.results_iframe_qc_trim, name="qc_iframe_trim"),
]
