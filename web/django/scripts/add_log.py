import logging
import datetime
from django.utils import timezone
from results.models import PipelineRun, Sample
from pipeline.models import PipelineLog


def run(*args):
    level = args[1]
    tool = args[2]
    message = args[4]
    sample = args[3]
    run_id = PipelineRun.objects.get(pk=args[0])
    if sample == "all":
        sample_id = None
    else:
        sample_id = Sample.objects.filter(run_id=run_id, name=sample)[0]

    log = PipelineLog(run=run_id,
                      sample=sample_id,
                      level=level,
                      tool=tool,
                      message=message,
                      timestamp=datetime.datetime.now(tz=timezone.utc))

    log.save()



