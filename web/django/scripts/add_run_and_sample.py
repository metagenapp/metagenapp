import logging
import os

import yaml
from django.utils import timezone

from results.models import PipelineRun, Sample, SampleParameter, SampleParameterPreset
from pipeline.models import PipelineTrimClip, PipelineTrim
from django.core.files import File
from pathlib import Path



def run(*args):
    logger = logging.getLogger("runscript.log")
    try:
        configfile = "tmp/config.yaml"
        with open(configfile) as fh:
            c = yaml.load(fh, Loader=yaml.SafeLoader)
            analysis_de = c.pop("analysis_de") if "analysis_de" in c.keys() else c["analysis"]
            descr = c.pop("description") if "description" in c.keys() else ""
            details = c.pop("details") if "details" in c.keys() else ""
            descr_de = c["description_de"] if "description_de" in c.keys() else descr
            details_de = c["details_de"] if "details_de" in c.keys() else details
            run = PipelineRun(analysis_en=c["analysis"],
                              analysis_de=analysis_de,
                              mode=c["mode"],
                              cpu_cores=c["max_cores"],
                              analysis_date=timezone.now(),
                              status="new",
                              details_en=details,
                              details_de=details_de,
                              description_en=descr,
                              description_de=descr_de,
                              image=args[0])
            run.save()

        trim = PipelineTrim.objects.create(run_id=run.pk,
                                           leading_quality=c["trim_leading"],
                                           trailing_quality=c["trim_trailing"],
                                           minimum_length=c["trim_minlength"],
                                           sliding_window=c["trim_sliding_window"],
                                           sliding_window_quality=c["trim_sliding_qual"],
                                           illumina_clipping=c["trim_illumina_clip"])

        trim.save()

        if c["trim_illumina_clip"] != "No Clipping":
            trimclip = PipelineTrimClip.objects.create(run_id=run.pk,
                                                       seed_mismatches=c["trim_seed_mismatches"],
                                                       palindrome_clip_theshold=c["trim_palindrome_clip_theshold"],
                                                       simple_clipping_theshold=c["trim_simple_clip_theshold"],
                                                       minimum_adapter_length=c["trim_minAdapterLength"])
            trimclip.save()

        presets = []

        def get_values(values, i):
            try:
                if type(values[i]) == list:
                    v_en = values[i][0]
                    v_de = values[i][1]
                else:
                    v_en = values[i]
                    v_de = values[i]
            except IndexError:
                v_en = None
                v_de = None

            return v_en, v_de

        for p in c["parameters"]:
            param = p["parameter"]
            if type(param) == list:
                param_en, param_de = param[0], param[1]
            else:
                param_en, param_de = param, param
            dtype = p["datatype"]
            values = p["values"]
            value_set = set()
            for v in values:
                if type(v) == list:
                    value_set.add(v[0])
                else:
                    value_set.add(v)

            if len(values) < 2 and dtype != "metric":
                raise Exception(
                    f"Sampleparameter values for parameter {param} out of range: At least two values are required!")
            if len(values) > 10:
                raise Exception(
                    f"Sampleparameter values for parameter {param} out of range: Maximum of 10 values exceeded!\nValues: {values}")
            if len(value_set) != len(values):
                raise Exception(f"Duplicate values for parameter {param}")
            else:
                v1, v1_de = get_values(values, 0)
                v2, v2_de = get_values(values, 1)
                v3, v3_de = get_values(values, 2)
                v4, v4_de = get_values(values, 3)
                v5, v5_de = get_values(values, 4)
                v6, v6_de = get_values(values, 5)
                v7, v7_de = get_values(values, 6)
                v8, v8_de = get_values(values, 7)
                v9, v9_de = get_values(values, 8)
                v10, v10_de = get_values(values, 9)

                preset = SampleParameterPreset.objects.create(run=run,
                                                              parameter_en=param_en,
                                                              parameter_de=param_de,
                                                              datatype=dtype,
                                                              value_1=v1,
                                                              value_2=v2,
                                                              value_3=v3,
                                                              value_4=v4,
                                                              value_5=v5,
                                                              value_6=v6,
                                                              value_7=v7,
                                                              value_8=v8,
                                                              value_9=v9,
                                                              value_10=v10,
                                                              value_1_de=v1_de,
                                                              value_2_de=v2_de,
                                                              value_3_de=v3_de,
                                                              value_4_de=v4_de,
                                                              value_5_de=v5_de,
                                                              value_6_de=v6_de,
                                                              value_7_de=v7_de,
                                                              value_8_de=v8_de,
                                                              value_9_de=v9_de,
                                                              value_10_de=v10_de)
                preset.save()
                presets.append(preset)

        for sample in c["samples"]:
            s = Sample.objects.create(run=run,
                                      name=sample["name"],
                                      sample_file_fwd=os.path.join("input", sample["files"][0]),
                                      sample_file_rev=os.path.join("input", sample["files"][1]))
            s.save()
            for preset in presets:
                if preset.datatype != "metric":
                    if preset.value_1 == str(sample[preset.parameter_en]):
                        val, order = preset.value_1, 1
                        val_de = preset.value_1_de
                    elif preset.value_2 == str(sample[preset.parameter_en]):
                        val, order = preset.value_2, 2
                        val_de = preset.value_2_de
                    elif preset.value_3 == str(sample[preset.parameter_en]):
                        val, order = preset.value_3, 3
                        val_de = preset.value_3_de
                    elif preset.value_4 == str(sample[preset.parameter_en]):
                        val, order = preset.value_4, 4
                        val_de = preset.value_4_de
                    elif preset.value_5 == str(sample[preset.parameter_en]):
                        val, order = preset.value_5, 5
                        val_de = preset.value_5_de
                    elif preset.value_6 == str(sample[preset.parameter_en]):
                        val, order = preset.value_6, 6
                        val_de = preset.value_6_de
                    elif preset.value_7 == str(sample[preset.parameter_en]):
                        val, order = preset.value_7, 7
                        val_de = preset.value_7_de
                    elif preset.value_8 == str(sample[preset.parameter_en]):
                        val, order = preset.value_8, 8
                        val_de = preset.value_8_de
                    elif preset.value_9 == str(sample[preset.parameter_en]):
                        val, order = preset.value_9, 9
                        val_de = preset.value_9_de
                    elif preset.value_10 == str(sample[preset.parameter_en]):
                        val, order = preset.value_10, 10
                        val_de = preset.value_10_de
                    else:
                        Exception(f"Parameter value {sample[preset.parameter_en]} is not Defined in PresetValues")

                if preset.datatype == "categorical":
                    order = None

                else:
                    val = str(sample[preset.parameter_en])
                    order = None
                    val_de = val

                sampleparam = SampleParameter.objects.create(sample=s,
                                                             run=run,
                                                             name_en=preset.parameter_en,
                                                             name_de=preset.parameter_de,
                                                             value=val,
                                                             value_de=val_de,
                                                             datatype=preset.datatype,
                                                             order=order)
                sampleparam.save()

    except Exception as e:
        logger.error(e)

    return 0

