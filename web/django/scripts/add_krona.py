import logging
from pathlib import Path

from results.models import Sample, PipelineRun, KronaPlot


def run(*args):
    sample_name = args[0]
    run_id = PipelineRun.objects.filter(pk=args[1])[0]
    sample_id = Sample.objects.filter(run_id=run_id, name=sample_name)[0]
    init = args[2]
    dir = args[3]
    if init == "worker":
        containerpath = f"{dir}input/{run_id.analysis_en}/krona"
    else:
        containerpath = f"{dir}input/krona"

    html = Path(f"{containerpath}/{sample_name}/{sample_name}_kronaplot.html")

    with open(html) as fh:
        data = fh.read()

    kronaplot = KronaPlot(run_id=run_id,
                          sample_id=sample_id,
                          kronahtml=data)
    kronaplot.save()



