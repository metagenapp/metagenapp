import json
import ast
import logging
import traceback
from collections import OrderedDict
from pathlib import Path

from results.models import PipelineRun, EmperorPlot, SampleParameterPreset


def run(*args):
    logger = logging.getLogger("runscript.log")
    def get_next(taxonomy_list, index):
        """
        recursive function creating correct plot label for taxonomy strings of silva database
        :param taxonomy_list: list of taxonomy levels (string separated by ":"
        :param index: current list index
        :return: label string
        """
        current_rank = taxonomy_list[index]
        if current_rank[5:] == "uncultured bacterium":
            next_rank = taxonomy_list[index - 1][5:]
            if next_rank in ["uncultured bacterium", "uncultured"]:
                return get_next(taxonomy_list, index - 1)
            else:
                return f"uncultured bacteria of {phylo_dict[index - 1]} {next_rank}"

        elif current_rank == "__":
            return f"Unclassified {phylo_dict[i]}"

        else:
            return current_rank[5:]

    run_id = PipelineRun.objects.get(pk=args[0])
    init = args[1]
    directory = str(args[2])
    if init == "worker":
        container_path = f"{directory}input/{run_id.analysis_en}/emperor_plots"
    else:
        container_path = f"{directory}input/emperor_plots"

    try:
        for i, rank in enumerate(["P", "C", "O", "F", "G", "S"], start=1):
            phylo_dict = {1: "phylum", 2: "class", 3: "order", 4: "family", 5: "genus", 6: "species"}
            html = Path(f"{container_path}/emperorplot_{rank}/emperor.html")

            with open(html) as fh:
                data = fh.read()
                data = data.split("var data = ")[1].lstrip()
                data = data.split(";\n\n  var plot, biplot = null, ec;")[0]
                data = data.replace("'", "u0027")

                data = json.loads(data, object_pairs_hook=OrderedDict)
                for ii, label in enumerate(data["biplot"]["decomposition"]["sample_ids"]):
                    if label.startswith("D_0__"):  # change labels for silva database
                        label = get_next(label.split(";"), i)

                    elif label.startswith("Unassigned"):
                        label = "Unassigned"

                    data["biplot"]["decomposition"]["sample_ids"][ii] = label
                    data["biplot"]["metadata"][ii][0] = label
                data = json.dumps(data).replace("None", "null")

            data_de = data
            params = SampleParameterPreset.objects.filter(run_id=run_id)

            for param in params:
                data_de = data_de.replace(param.parameter_en, param.parameter_de)
                data_de = data_de.replace(f'"{param.value_1}"', f'"{param.value_1_de}"')
                data_de = data_de.replace(f'"{param.value_2}"', f'"{param.value_2_de}"')
                data_de = data_de.replace(f'"{param.value_3}"', f'"{param.value_3_de}"')
                data_de = data_de.replace(f'"{param.value_4}"', f'"{param.value_4_de}"')
                data_de = data_de.replace(f'"{param.value_5}"', f'"{param.value_5_de}"')
                data_de = data_de.replace(f'"{param.value_6}"', f'"{param.value_6_de}"')
                data_de = data_de.replace(f'"{param.value_7}"', f'"{param.value_7_de}"')
                data_de = data_de.replace(f'"{param.value_8}"', f'"{param.value_8_de}"')
                data_de = data_de.replace(f'"{param.value_9}"', f'"{param.value_9_de}"')
                data_de = data_de.replace(f'"{param.value_10}"', f'"{param.value_10_de}"')

            EmperorPlot(run_id=run_id, rank=rank, emperordata_en=data, emperordata_de=data_de).save()

    except FileNotFoundError:
        return "No Emperorplots found!"







