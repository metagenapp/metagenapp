import logging
from pathlib import Path
from collections import defaultdict
from results.models import Sample, PipelineRun, Taxon
from django.db import transaction


def run(*args):
    sample_name = args[0]
    run_id = PipelineRun.objects.get(pk=args[1])
    init = args[2]
    dir = args[3]
    sample_id = Sample.objects.filter(run_id=run_id, name=sample_name)[0]
    with open("/db/silva_qiime/taxonomy_7_levels.txt") as taxonomy:
        mapping_dict = {line.strip().split("\t")[1]: line.split("\t")[0] for line in taxonomy.readlines()}
    if init == "worker":
        containerpath = f"{dir}input/{run_id.analysis_en}"
    else:
        containerpath = f"{dir}input"

    classifcation = Path(f"{containerpath}/qiime_silva/{sample_name}/taxonomy.tsv")

    ranks = {0: "D", 1: "P", 2: "C", 3: "O", 4: "F", 5: "G", 6: "S", -1: "R", -2: "U", -3: "a"}
    last_parents = defaultdict(Taxon)
    last_parents[-3] = Taxon.objects.create(percentage=0.0,
                                            run_id=run_id,
                                            sample_id=sample_id,
                                            fragments_root=0,
                                            fragments=0,
                                            abundance=0,
                                            rank=ranks[-3],
                                            tax_id=0,
                                            name="all")
    last_parents[-3].save()
    last_parents[-1] = Taxon.objects.create(percentage=0.0,
                                            run_id=run_id,
                                            sample_id=sample_id,
                                            fragments_root=0,
                                            fragments=0,
                                            abundance=0,
                                            rank=ranks[-1],
                                            tax_id=0,
                                            name="Root",
                                            parent=last_parents[-3])
    last_parents[-1].save()
    with open(classifcation) as fh:
        names_used = set()
        fh.readline()  # skip header
        for line in fh.readlines():
            line = line.split("\t")
            length = len(line[1].split(";"))
            for i, t in enumerate(line[1].split(";"), start=1):
                if i == length:
                    try:
                        feature_id = mapping_dict[line[1]]
                        name = feature_id + " " + t[5:]
                    except KeyError:
                        feature_id = 0
                        name = t[5:] if t != "Unassigned" else t
                else:
                    feature_id = 0
                    name = t[5:] if t != "Unassigned" else t

                if name not in names_used:
                    r = -2 if t[2] == "a" else int(t[2])
                    taxon = Taxon.objects.create(percentage=0.0,
                                                 run_id=run_id,
                                                 sample_id=sample_id,
                                                 fragments_root=0,
                                                 fragments=0,
                                                 abundance=0,
                                                 rank=ranks[r],
                                                 tax_id=feature_id,
                                                 name=name,
                                                 parent=last_parents[r - 1])
                    taxon.save()
                    names_used.add(name)
                else:
                    taxon = Taxon.objects.get(run_id=run_id,
                                              sample_id=sample_id,
                                              name=name)

                last_parents[r] = taxon

        for i, rank in enumerate(["D", "P", "C", "O", "F", "G", "S"], start=1):
            file = Path(f"{containerpath}/abundance_{i}/{sample_name}/ab_{i}.tsv")
            with open(file) as fh:
                fh.readline()
                fh.readline()
                for line in fh.readlines():
                    tax, abundance = line.strip().split("\t")
                    try:
                        feature_id = mapping_dict[tax]
                        name = feature_id + " " + [i for i in tax.split("__") if i != ""][-1]

                    except KeyError:
                        name = [i for i in tax.split("__") if i not in ["", ";"]][-1]
                        if name == "Unassigned":
                            rank = "U"

                    Taxon.objects.filter(sample_id=sample_id, run_id=run_id, rank=rank, name=name) \
                        .update(abundance=int(float(abundance)))

