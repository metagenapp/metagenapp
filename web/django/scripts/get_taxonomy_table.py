import logging
from pathlib import Path
from collections import defaultdict
from results.models import Sample, PipelineRun, Taxon
import pandas as pd


def run(*args):
    run = PipelineRun.objects.filter(pk=args[1])[0]
    init = args[2]
    dir = args[3]
    if init == "worker":
        path = f"{dir}input/{run.analysis_en}/qiime/"
    else:
        path = f"{dir}input/qiime/"

    table = pd.read_csv(f"{path}{args[0]}", header=0, index_col=0, sep="\t")
    species_d = table.transpose().ne(0).idxmax().transpose().to_dict()
    filename = f"{path}{args[0][:-4]}_taxonomy.tsv"
    with open(filename, "w") as outfile:
        outfile.write("\t".join(
            ["OTU", "Taxonomy", "superkingdom", "phylum", "class", "order", "family", "genus", "species",
             "strain"]) + "\n")
        for k, v in species_d.items():
            sample = Sample.objects.filter(name=v, run_id=run)[0]
            species = Taxon.objects.filter(name=k, sample_id=sample)[0]
            taxonomy = [t.name for t in Taxon.get_ancestors(species, include_self=True)]
            taxo_str = ";cellular organisms;" + ";".join(taxonomy[2:])
            line = "\t".join([species.name, taxo_str] + taxonomy[2:] + ["NA\n"])

            outfile.write(line)

