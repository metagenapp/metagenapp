import logging
from pathlib import Path
from collections import defaultdict
from results.models import Sample, PipelineRun, Taxon
from django.db import transaction


def run(*args):
    sample_name = args[0]
    run_id = PipelineRun.objects.filter(pk=args[1])[0]
    sample_id = Sample.objects.filter(run_id=run_id, name=sample_name)[0]
    init = args[2]
    dir =args[3]
    if init == "worker":
        containerpath = f"{dir}input/{run_id.analysis_en}/bracken"
    else:
        containerpath = f"{dir}input/bracken"

    kraken_report = Path(f"{containerpath}/{sample_name}/{sample_name}_kraken.report")

    with open(kraken_report) as fh:
        kraken_data = fh.readlines()

    last_parents = defaultdict(Taxon)
    taxon = Taxon.objects.create(percentage=0.0,
                                 run_id=run_id,
                                 sample_id=sample_id,
                                 fragments_root=0,
                                 fragments=0,
                                 abundance=0,
                                 rank="a",
                                 tax_id=0,
                                 name="all")
    taxon.save()
    last_parents[0] = taxon
    last_rank = 0
    with transaction.atomic():
        with Taxon.objects.disable_mptt_updates():
            for line in kraken_data:
                percentage, fragments_root, fragments, rank, tax_id, name = line.strip().split("\t")
                name_striped = name.lstrip(" ")
                if rank.startswith("U") or rank.startswith("R"):
                    rank_count = 1
                else:
                    rank_count = int((len(name) - len(name_striped)) / 2)

                parent = last_parents[rank_count - 1]
                taxon = Taxon.objects.create(percentage=percentage,
                                             run_id=run_id,
                                             sample_id=sample_id,
                                             fragments_root=fragments_root,
                                             fragments=fragments,
                                             abundance=0,
                                             rank=rank,
                                             tax_id=tax_id,
                                             name=name_striped,
                                             parent=parent)
                taxon.save()

                if last_rank < rank_count:
                    last_parents[rank_count] = taxon

        Taxon.objects.rebuild()

    # add abundances from bracken report for each rank:
    for rank in ["S", "G", "F", "O", "C", "P"]:
        file = Path(f"{containerpath}/{sample_name}/{sample_name}_bracken_{rank}.report")
        with open(file) as fh:
            header = fh.readline()
            for line in fh.readlines():
                line = line.split("\t")
                tax_id = int(line[1])
                new_est_reads = int(line[5])
                Taxon.objects.filter(sample_id=sample_id, run_id=run_id, tax_id=tax_id)\
                    .update(abundance=new_est_reads)

