import logging
from pathlib import Path

from results.models import Sample, PipelineRun, PipelineQC



def run(*args):
    sample_name = args[0]
    run_id = PipelineRun.objects.filter(pk=args[3])[0]
    sample_id = Sample.objects.filter(run_id=run_id, name=sample_name)[0]
    init = args[4]
    dir = args[5]
    if init == "worker":
        containerpath = f"{dir}input/{run_id.analysis_en}/fastqc"
    else:
        containerpath = f"{dir}input/fastqc"

    fwd_fq = Path(containerpath) / sample_name / args[1]
    rev_fq = Path(containerpath) / sample_name / args[2]

    fwd_html = str(fwd_fq).split(".")[0] + "_fastqc.html"
    rev_html = str(rev_fq).split(".")[0] + "_fastqc.html"

    fwd_html_trim = str(fwd_fq.parent) + f"/{sample_name}_fwd_trim_merged_fastqc.html"
    rev_html_trim = str(rev_fq.parent) + f"/{sample_name}_rev_trim_merged_fastqc.html"

    with open(fwd_html) as fwd_h:
        fwd_data = fwd_h.read()
    with open(rev_html) as rev_h:
        rev_data = rev_h.read()
    with open(fwd_html_trim) as fwd_h_t:
        fwd_data_trim = fwd_h_t.read()
    with open(rev_html_trim) as rev_h_t:
        rev_data_trim = rev_h_t.read()

    qc_fwd = PipelineQC(run_id=run_id,
                        sample_id=sample_id,
                        fastqc_fwd=fwd_data,
                        fastqc_rev=rev_data,
                        fastqc_fwd_trim=fwd_data_trim,
                        fastqc_rev_trim=rev_data_trim)
    qc_fwd.save()

