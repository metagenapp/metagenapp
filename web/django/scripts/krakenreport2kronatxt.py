import logging
from results.models import Taxon, Sample, PipelineRun


def run(*args):
    run = PipelineRun.objects.filter(pk=args[0])[0]
    sample = Sample.objects.filter(run_id=run, name=args[1])[0]
    taxons = Taxon.objects.filter(sample_id=sample)
    init = args[2]
    dir = args[3]

    if init == "worker":
        path = f"{dir}input/{run.analysis_en}/bracken/{sample.name}/"
    else:
        path = f"{dir}input/bracken/{sample.name}/"

    filename = f"{path}{args[1]}_kraken2krona.txt"
    with open(filename, "w") as outfile:
        for taxon in taxons:
            if not taxon.rank.startswith(("R", "a")):
                taxonomy = [t.name for t in Taxon.get_ancestors(taxon, include_self=True) if t.name != "cellular organisms"][1:]
                outfile.write("\t".join([str(taxon.fragments)] + taxonomy) + "\n")

