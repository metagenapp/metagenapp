import logging
from zipfile import ZipFile, ZIP_DEFLATED
from pathlib import Path
import shutil

from django.core import serializers

from pipeline.models import *
from results.models import *


def run(*args):
    logger = logging.getLogger("runscript.log")
    try:
        run = PipelineRun.objects.get(pk=args[0])
        init = args[1]
        dir = args[2]
        if init == "worker":
            path = f"{dir}input/{run.analysis_en}"
        else:
            path = f"{dir}input"

        objects = list(PipelineRun.objects.filter(pk=args[0]))
        objects += list(Sample.objects.filter(run_id=run))
        objects += list(SampleParameterPreset.objects.filter(run=run))
        objects += list(SampleParameter.objects.filter(run_id=run))
        objects += list(PipelineTrim.objects.filter(run=run))
        objects += list(PipelineTrimClip.objects.filter(run=run))
        objects += list(PipelineLog.objects.filter(run=run))
        objects += list(PipelineQC.objects.filter(run_id=run))
        objects += list(KronaPlot.objects.filter(run_id=run))
        objects += list(EmperorPlot.objects.filter(run_id=run))

        taxonomys = Taxon.objects.filter(run_id=run, name="all")
        for taxonomy in taxonomys:
            objects += list(taxonomy.get_descendants(include_self=True))

        xml = f"{path}/{run.analysis_en}_results.xml"
        with open(xml, "w") as f:
            f.write(serializers.serialize("xml", objects))

        zip_fp = f"{path}/{run.analysis_en}_results.zip"
        out_dirs = ["abundance_tables",
                    "emperor_plots",
                    "bracken",
                    "fastqc",
                    "krona"]

        with ZipFile(zip_fp, "w") as zip_fh:
            zip_fh.write(xml, arcname=f"{run.analysis_en}_results.xml", compress_type=ZIP_DEFLATED)
            os.remove(xml)

            if run.image is not "None":
                zip_fh.write(run.image.path,
                             arcname=Path(run.image.path).name,
                             compress_type=ZIP_DEFLATED)

            for dirname in out_dirs:
                directory = Path(path)/dirname
                for filepath, dirs, files in os.walk(str(directory)):
                    archive_dir = os.path.relpath(filepath, "input")
                    for file in files:
                        zip_fh.write(os.path.join(filepath, file),
                                     arcname=os.path.join(archive_dir, file),
                                     compress_type=ZIP_DEFLATED)

                shutil.rmtree(directory, ignore_errors=True)

    except Exception as e:
        logger.error(e)
