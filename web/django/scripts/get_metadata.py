import logging
from results.models import Sample, PipelineRun, SampleParameterPreset, SampleParameter
import pandas as pd
import os


def run(*args):
    run = PipelineRun.objects.get(pk=args[0])
    samples = Sample.objects.filter(run_id=run)
    presets = SampleParameterPreset.objects.filter(run_id=run)
    header = ["#SampleID"] + [p.parameter_en for p in presets]
    types = [p.datatype.replace("metric", "numeric") for p in presets]
    types = [p.replace("ordinal", "categorical") for p in types]
    types = ["#q2:types"] + types

    array = [types]

    init = args[1]
    dir = args[2]
    if init == "worker":
        path = f"{dir}input/{run.analysis_en}/qiime/map.txt"
    else:
        path = f"{dir}input/qiime/map.txt"

    for sample in samples:
        line = [sample.name]
        for i, preset in enumerate(presets):
            params = SampleParameter.objects.filter(sample_id=sample, name_en=preset.parameter_en)
            values = params.values_list("value", flat=True)[0]
            if types[i + 1] == "numeric":
                values = values.replace("NA", "")
            line.append(values)
        array.append(line)

    df = pd.DataFrame(array)
    df.columns = header
    df = df.set_index(["#SampleID"])
    os.makedirs(path[:-8], exist_ok=True)
    df.to_csv(path, sep="\t")
