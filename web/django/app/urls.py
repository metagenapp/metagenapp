from django.urls import path, re_path, include
from django.conf.urls.static import static
from django.conf import settings

from . import views

app_name = "app"
urlpatterns = [
    path("", views.index_view, name="index"),
    re_path(r'^ajax/request_lock/$', views.request_lock, name='request_lock')] \
              + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
#               + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \


