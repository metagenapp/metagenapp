from django.conf import settings
from django.shortcuts import redirect
from results.forms import SampleMenu
from pipeline.forms import StepsMenu
from results.models import Sample, EmperorPlot
from pipeline.models import PipelineLog
from django.utils import translation


class SessionController:
    def __init__(self, request, lang=None):
        """
        Initialize the app_session.
        """
        if lang:
            translation.activate(lang)
            request.session[translation.LANGUAGE_SESSION_KEY] = lang
        self.session = request.session
        app_session = self.session.get(settings.APP_SESSION_ID)

        if not app_session:
            # save an empty app_session in the session
            app_session = self.session[settings.APP_SESSION_ID] = {"sn_lock": False,
                                                                   "active_run": None,
                                                                   "emperor": False,
                                                                   "active_samples": [],
                                                                   "active_logs": [],
                                                                   "all_samples_active": "",
                                                                   "all_logs_active": ""}
        self.app_session = app_session

    @property
    def sn_lock(self):
        return self.app_session["sn_lock"]

    @sn_lock.setter
    def sn_lock(self, value):
        self.app_session["sn_lock"] = value

    @property
    def active_run(self):
        return self.app_session["active_run"]

    @active_run.setter
    def active_run(self, value):
        self.app_session["active_run"] = str(value) if value else None
        self.app_session["emperor"] = True if len(EmperorPlot.objects.filter(run_id=value)) > 0 else False

    @property
    def emperor(self):
        return self.app_session["emperor"]

    @property
    def active_samples(self):
        return self.app_session["active_samples"]

    @property
    def active_logs(self):
        return self.app_session["active_logs"]

    @property
    def all_samples_active(self):
        return self.app_session["all_samples_active"]

    @property
    def all_logs_active(self):
        return self.app_session["all_logs_active"]

    @active_samples.setter
    def active_samples(self, id_list):
        self.app_session["active_samples"] = id_list
        if len(id_list) == len(Sample.objects.filter(run_id=self.active_run)):
            self.app_session["all_samples_active"] = "checked"
        else:
            self.app_session["all_samples_active"] = ""

    @active_logs.setter
    def active_logs(self, step_list):
        self.app_session["active_logs"] = step_list
        # steps = PipelineLog.objects.filter(run_id=self.app_session["active_run"])\
        #     .order_by("timestamp").only("id", "tool")
        # if len(step_list) == len(steps):
        #     self.app_session["all_logs_active"] = "checked"
        # else:
        #     self.app_session["all_logs_active"] = ""

    def save(self):
        # mark the session as "modified" to make sure it gets saved
        self.session.modified = True


def redirect_run(function):
    def wrapper(request, *args, **kwargs):
        session = SessionController(request)
        if session.active_run:
            return redirect(f"{request.path}{session.active_run}")

        return function(request, *args, **kwargs)

    return wrapper


def update_session(function):
    def wrapper(request, *args, **kwargs):
        session = SessionController(request)
        try:
            session.active_run = kwargs["run_id"]
        except KeyError:
            pass

        if request.method == "POST":
            samples = Sample.objects.filter(run_id=session.active_run)
            samples_form = SampleMenu(request.POST, qs=samples, prefix="samples_form")
            if samples_form.is_valid():
                session.active_samples = samples_form.cleaned_data["samples"]

        return function(request, *args, **kwargs)

    return wrapper


def update_steps(function):
    def wrapper(request, *args, **kwargs):
        session = SessionController(request)
        try:
            session.active_run = kwargs["run_id"]
        except KeyError:
            pass

        if request.method == "POST":
            steps = PipelineLog.objects.filter(run_id=session.active_run).order_by("timestamp").only("id", "tool")
            steps_form = StepsMenu(request.POST, qs=steps, prefix="steps_form")
            if steps_form.is_valid():
                session.active_logs = steps_form.cleaned_data["step"]

        return function(request, *args, **kwargs)

    return wrapper


def no_sidnav_fix(function):
    def wrapper(request, *args, **kwargs):
        session = SessionController(request)
        session.sn_lock = False

        return function(request, *args, **kwargs)

    return wrapper



