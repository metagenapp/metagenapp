var sidebar = $('.sidenav');
var update_sidenav = function (data) {
    var trigger = $('#trigger-icon');
    var on = '<i class="material-icons teal-text" style="font-size:2rem;margin-right:0">toggle_on</i>';
    var off = '<i class="material-icons grey-text darken-3" style="font-size:2rem;margin-right:0">toggle_off</i>';
    if (data.response === true || data === true) {
        $('#lock-toggle').html(on);
        $('#id_lock').attr('value', "True");
        sidebar.addClass("sidenav-fixed");
        sidebar.sidenav();
        $("header").addClass("padding-300");
        $("main").addClass("padding-300");
        $("footer").addClass("padding-300");
    } else {
        $('#lock-toggle').html(off);
        $('#id_lock').attr('value', "False");
        sidebar.removeClass("sidenav-fixed");
        sidebar.sidenav();
        trigger.show();
        $("header").removeClass("padding-300");
        $("main").removeClass("padding-300");
        $("footer").removeClass("padding-300");
        trigger.click()
    }
};

var update_lock = function (post, callback) {
    is_locked = $('#id_lock').attr("value");
    $.ajax({
        url: '{% url "app:request_lock" %}',
        data: {
            'is_locked': is_locked
        },
        dataType: 'json',
        type: "POST",
        success: callback
    });
};

$(document).ready(function () {
    sidebar.sidenav();
    $('.collapsible').collapsible();
    $('.datepicker').datepicker({'format': 'yyyy-mm-dd'});
    $('select').formSelect();
});


$("#lock-side-nav").click(function () {
    update_lock(true, update_sidenav)
});

var addEvent = function (object, type, callback) {
    if (object == null || typeof (object) == 'undefined') return;
    if (object.addEventListener) {
        object.addEventListener(type, callback, false);
    } else if (object.attachEvent) {
        object.attachEvent("on" + type, callback);
    } else {
        object["on" + type] = callback;
    }
};

addEvent(window, "resize", function () {
    var sidenav = $('.sidenav');
    if ($(window).width() < 992 || sidenav.hasClass("sidenav-fixed")) {
        $('#trigger-icon').show();
    }
});