from django import forms


class LockForm(forms.Form):
    lock = forms.BooleanField(widget=forms.HiddenInput)

    def __init__(self, request, *args, **kwargs):
        super(LockForm, self).__init__(*args, **kwargs)
        try:
            value = True if request.session["sn_lock"] is False else False
        except KeyError:  # cookie deleted
            value = True
        self.fields["lock"].initial = value