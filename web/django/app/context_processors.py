from .session_controller import SessionController
from .forms import LockForm


def session(request):
    return {"session": SessionController(request)}


def lock_form(request):
    return {"lock_form": LockForm(request)}
