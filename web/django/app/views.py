from django.shortcuts import render, redirect
from django.http import JsonResponse
from .session_controller import SessionController, no_sidnav_fix
from results.models import PipelineRun
from django.utils import translation


@no_sidnav_fix
def index_view(request):
    template_name = "app/base.html"
    runs = PipelineRun.objects.filter(status="done")

    return render(request, template_name, {"runs": runs})


def request_lock(request):
    session = SessionController(request)
    data = {"sn_lock": request.session.get("sn_lock")}
    if request.is_ajax():
        lock_val = request.POST.get("is_locked")
        session.sn_lock = True if lock_val == "False" else False
        session.save()
        data.update(response=session.sn_lock)

    return JsonResponse(data)

