import math

from django import forms
from django.forms import ValidationError
from django.forms import modelformset_factory, formset_factory, Select
from .models import Sample, PipelineRun, PipelineLog,  SampleParameter, SampleParameterPreset, PipelineTrim, PipelineTrimClip
from results.forms import SampleMenuRadio, SampleMenu
from django.utils.translation import gettext_lazy as _


class ConfigForm(forms.ModelForm):
    class Meta:
        model = PipelineRun
        fields = ("analysis_en",
                  "analysis_de",
                  "mode", "cpu_cores",
                  "description_en",
                  "description_de",
                  "image",
                  "details_en",
                  "details_de")
        widgets = {'analysis_en': forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': _('maximum: 20 characters')
        }),
            'analysis_de': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': _('maximum: 20 characters (optional)')
            }),
            'description_en': forms.TextInput(attrs={
                'class': 'form-control expandable',
                'placeholder': _('maximum: 500 characters')
            }),
            'description_de': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': _('maximum: 500 characters')
            }),
            'details_en': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': _('maximum: 10000 characters')
            }),
            'details_de': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': _('maximum: 10000 characters')
            }),
        }


class RunForm(forms.ModelForm):
    class Meta:
        model = PipelineRun
        fields = ("analysis_en", "status", "description_en", "description_de", "image")
        widgets = {'image': forms.FileInput(attrs={
        }),
        }


class TrimForm(forms.ModelForm):
    class Meta:
        model = PipelineTrim
        fields = ("leading_quality", "trailing_quality", "minimum_length", "sliding_window", "sliding_window_quality",
                  "illumina_clipping", "crop", "head_crop")


class TrimClipForm(forms.ModelForm):
    class Meta:
        model = PipelineTrimClip
        fields = ("seed_mismatches", "palindrome_clip_theshold", "simple_clipping_theshold", "minimum_adapter_length")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        if self.instance and self.instance.minimum_adapter_length is not None:
            new_choices = list(self.fields['minimum_adapter_length'].choices)
            new_choices[0] = (0, _("No min length"))
            self.fields['minimum_adapter_length'].choices = new_choices
            self.fields['minimum_adapter_length'].widget.choices = new_choices


SampleFormSet = modelformset_factory(
    Sample,
    fields=('name', "sample_file_fwd", "sample_file_rev"),
    extra=1,
    widgets={
        'name': forms.TextInput(attrs={
            'class': 'form-control',
            'placeholder': _('Enter sample name here')
        }
        ),
        "sample_file_fwd": forms.FileInput(attrs={'class': 'form-control', 'placeholder': 'Fastq forward'}),
        "sample_file_rev": forms.FileInput(attrs={'class': 'form-control', 'placeholder': 'Fastq reverse'})
    }
)


class SampleParameterPresetForm(forms.ModelForm):
    class Meta:
        model = SampleParameterPreset
        fields = ('parameter_en', 'parameter_de', "datatype",
                  "value_1", "value_2", "value_3", "value_4", "value_5", "value_6", "value_7", "value_8", "value_9", "value_10",
                  "value_1_de", "value_2_de", "value_3_de", "value_4_de", "value_5_de", "value_6_de", "value_7_de",
                  "value_8_de", "value_9_de", "value_10_de")
        widgets = {
            'parameter_en': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': _('Name of Parameter')
            }
            ),
            'parameter_de': forms.TextInput(attrs={
                'class': 'form-control',
                'placeholder': _('Name of Parameter')
            }),
            "value_1": forms.TextInput(
                attrs={'class': 'form-control s10ths',
                       'placeholder': _('required')}),
            "value_2": forms.TextInput(
                attrs={'class': 'form-control s10ths',
                       'placeholder': _('required')}),
            "value_3": forms.TextInput(
                attrs={'class': 'form-control s10ths',
                       'placeholder': 'optional'}),
            "value_4": forms.TextInput(
                attrs={'class': 'form-control s10ths',
                       'placeholder': 'optional'}),
            "value_5": forms.TextInput(
                attrs={'class': 'form-control s10ths',
                       'placeholder': 'optional'}),
            "value_6": forms.TextInput(
                attrs={'class': 'form-control s10ths',
                       'placeholder': 'optional'}),
            "value_7": forms.TextInput(
                attrs={'class': 'form-control s10ths',
                       'placeholder': 'optional'}),
            "value_8": forms.TextInput(
                attrs={'class': 'form-control s10ths',
                       'placeholder': 'optional'}),
            "value_9": forms.TextInput(
                attrs={'class': 'form-control s10ths',
                       'placeholder': 'optional'}),
            "value_10": forms.TextInput(
                attrs={'class': 'form-control s10ths',
                       'placeholder': 'optional'}),
            "value_1_de": forms.TextInput(
                attrs={'class': 'form-control s10ths',
                       'placeholder': _('optional')}),
            "value_2_de": forms.TextInput(
                attrs={'class': 'form-control s10ths',
                       'placeholder': _('optional')}),
            "value_3_de": forms.TextInput(
                attrs={'class': 'form-control s10ths',
                       'placeholder': 'optional'}),
            "value_4_de": forms.TextInput(
                attrs={'class': 'form-control s10ths',
                       'placeholder': 'optional'}),
            "value_5_de": forms.TextInput(
                attrs={'class': 'form-control s10ths',
                       'placeholder': 'optional'}),
            "value_6_de": forms.TextInput(
                attrs={'class': 'form-control s10ths',
                       'placeholder': 'optional'}),
            "value_7_de": forms.TextInput(
                attrs={'class': 'form-control s10ths',
                       'placeholder': 'optional'}),
            "value_8_de": forms.TextInput(
                attrs={'class': 'form-control s10ths',
                       'placeholder': 'optional'}),
            "value_9_de": forms.TextInput(
                attrs={'class': 'form-control s10ths',
                       'placeholder': 'optional'}),
            "value_10_de": forms.TextInput(
                attrs={'class': 'form-control s10ths',
                       'placeholder': 'optional'}),
            "datatype": Select(choices=(("categorical", _("categorical")),
                                        ("ordinal", "ordinal"),
                                        ("metric", _("metric"))),
                               attrs={"class": "form-control"})
        }

    def clean(self):
        cleaned_data = super().clean()

        def catch_val_err(self, val, missing):
            if not missing:
                try:
                    return float(cleaned_data.get(val))
                except (ValueError, TypeError):
                    self.add_error(val, _("Not numeric"))

        def catch_missing(self, val):
            if not cleaned_data.get(val):
                self.add_error(val, _("Missing value"))
                return True
            else:
                return False

        dt = cleaned_data.get("datatype")
        missing_min = catch_missing(self, "value_1")
        missing_max = catch_missing(self, "value_2")

        if dt == "metric":
            missing_stp = catch_missing(self, "value_3")
            min = catch_val_err(self, "value_1", missing_min)
            max = catch_val_err(self, "value_2", missing_max)
            stp = catch_val_err(self, "value_3", missing_stp)

            if None not in [min, max]:
                if min > max:
                    self.add_error("value_2", "min greater than max")

                elif stp is not None:
                    if stp > (max - min):
                        self.add_error("value_3", "Step value greater than min-max range.")

        return cleaned_data


class BaseSampleParameterPresetFormSet(forms.BaseFormSet):
    def clean(self):
        params = []
        for form in self.forms:
            try:
                param = form.cleaned_data["Parameter"]
                if param in params:
                    self.errors.append({"all": f'Parameter name "{param}" used more than once!'})
                    raise ValidationError("all", "Parameter name already used")
                params.append(param)

            except KeyError:
                pass

        return self.errors


SampleParameterPresetFormSet = modelformset_factory(SampleParameterPreset,
                                                    form=SampleParameterPresetForm,
                                                    formset=BaseSampleParameterPresetFormSet,
                                                    extra=1)


class SampleParameterForm(forms.Form):
    def __init__(self, run_id, sample_id, *args, **kwargs):
        self.presets = SampleParameterPreset.objects.filter(run_id=run_id)
        super(SampleParameterForm, self).__init__(*args, **kwargs)
        CHOICES = []
        for p in self.presets:
            if p.datatype == "metric":
                min = float(p.value_1)
                max = float(p.value_2)
                stp = float(p.value_3)
                x = min
                while x < max:
                    CHOICES.append(("{0:g}".format(x), "{0:g}".format(x)))
                    x += stp
            else:
                CHOICES.append((p.value_1 + "1", p.value_1))
                CHOICES.append((p.value_2 + "2", p.value_2))
                if p.value_3:
                    CHOICES.append((p.value_3 + "3", p.value_3))
                if p.value_4:
                    CHOICES.append((p.value_4 + "4", p.value_4))
                if p.value_5:
                    CHOICES.append((p.value_5 + "5", p.value_5))
                if p.value_6:
                    CHOICES.append((p.value_5 + "5", p.value_5))
                if p.value_7:
                    CHOICES.append((p.value_5 + "5", p.value_5))
                if p.value_8:
                    CHOICES.append((p.value_5 + "5", p.value_5))
                if p.value_9:
                    CHOICES.append((p.value_5 + "5", p.value_5))
                if p.value_10:
                    CHOICES.append((p.value_5 + "5", p.value_5))

            self.fields[p.parameter_en] = forms.ChoiceField(choices=CHOICES)
            self.fields["sample_id"] = forms.CharField(initial=sample_id,
                                                       widget=forms.HiddenInput())
            self.fields[p.parameter_en + "_dt"] = forms.CharField(initial=p.datatype,
                                                               widget=forms.HiddenInput())

            print(self.fields)


SampleParameterFormSet = formset_factory(SampleParameterForm)


class StepsMenu(forms.Form):
    class Meta:
        model = PipelineLog
        fields = ("name",)

    def __init__(self, *args, qs=None, session=None,  **kwargs):
        super(StepsMenu, self).__init__(*args, **kwargs)
        initial = session.active_logs if session else []
        print(session)
        if qs:
            tools = []
            choices = []
            for x in qs:
                if x.tool not in tools:
                    choices.append((x.tool, x.tool))
                tools.append(x.tool)
            self.fields['step'] = forms.MultipleChoiceField(choices=choices,
                                                             widget=forms.CheckboxSelectMultiple(),
                                                             label=tools,
                                                             required=False,
                                                             initial=initial)


class UploadMenu(forms.Form):
    xml_file = forms.FileField(label=_("Browse Files"))


