function updateElementIndex(el, prefix, ndx) {
    var id_regex = new RegExp('(' + prefix + '-\\d+)');
    var replacement = prefix + '-' + ndx;
    if ($(el).attr("for")) $(el).attr("for", $(el).attr("for").replace(id_regex, replacement));
    if (el.id) el.id = el.id.replace(id_regex, replacement);
    if (el.name) el.name = el.name.replace(id_regex, replacement);
}

function cloneMore(selector, prefix) {
    // console.log(selector);
    // console.log(prefix);
    var newElement = $(selector).clone(true, true);
    var total = $('#id_' + prefix + '-TOTAL_FORMS');
    var total_val = total.val();
    newElement.find(':input').each(function () {
        var name = $(this).attr('name');
        if (name) {
            name = name.replace('-' + (total_val - 1) + '-', '-' + total_val + '-');
            var id = 'id_' + name;
            //
            $(this).attr({'name': name, 'id': id}).val('').removeAttr('checked');
        }
        $(this).filter("[class~='select-dropdown'").attr("id", "cloned");
    });

    total_val++;
    total.val(total_val);
    $(selector).after(newElement);
    var conditionRow = $('.form-row').eq(-2);
    conditionRow.find('.btn.add-form-row')
        .addClass('red').addClass('lighten-2')
        .removeClass('add-form-row').addClass('remove-form-row')
        .html('-');
    var newRow = $('.form-row:last');
    newRow.find("select").val(conditionRow.find("option:selected").text());
    newRow.find("select").formSelect();
    var cloned = $('#cloned').parent();
    var new_wapper = cloned.find(".select-wrapper");
    new_wapper.find("option:first").addClass("selected");
    cloned.replaceWith(new_wapper);
}

function deleteForm(prefix, btn) {
    // console.log(prefix);
    var total = parseInt($('#id_' + prefix + '-TOTAL_FORMS').val());
    // console.log(total);
    if (total > 1) {
        btn.closest('.form-row').remove();
        var forms = $('.form-row');
        $('#id_' + prefix + '-TOTAL_FORMS').val(forms.length);
        for (var i = 0, formCount = forms.length; i < formCount; i++) {
            $(forms.get(i)).find(':input').each(function () {
                updateElementIndex(this, prefix, i);
            });
        }
    }
}

$(document).on('click', '.add-form-row', function (e) {
    e.preventDefault();
    cloneMore('.form-row:last', 'form');
});

$(document).on('click', '.remove-form-row', function (e) {
    e.preventDefault();
    deleteForm('form', $(this));
});

$('select').change( function (e) {
    var language =  $("a[data-target='lang']").text().slice(-3,-1);
    var row = $("div.pval");

    if (language === "en"){
         if ($(this).find(":selected").text() === "metric") {
        row.find("div:nth-child(1)").find("label").text("min value");
        row.find("div:nth-child(2)").find("label").text("max value");
        row.find("div:nth-child(3)").find("label").text("interval");
        row.find("div:nth-child(3)").find("input").attr("placeholder", "required");
        row.find("div.col.s1.s10ths.input-field:gt(2)").hide();
    } else {
        row.find("div.col.s1.s10ths.input-field:gt(2)").show();
        for (let i=1; i < 11; i++) {
            row.find("div:nth-child("+i+")").find("label").text("Value " + i);
        }
        for (let i=11; i < 21; i++) {
            row.find("div:nth-child("+i+")").find("label").text("Value " + (i-10) + " (ge)");
        }
        row.find("div:nth-child(3)").find("input").attr("placeholder", "optional");
    }
    }
    if (language === "de"){
         if ($(this).find(":selected").text() === "metrisch") {
        row.find("div:nth-child(1)").find("label").text("min Wert");
        row.find("div:nth-child(2)").find("label").text("max Wert");
        row.find("div:nth-child(3)").find("label").text("Intervall");
        row.find("div:nth-child(3)").find("input").attr("placeholder", "Pflichtfeld");
        row.find("div.col.s1.s10ths.input-field:gt(2)").hide();
    } else {
        row.find("div.col.s1.s10ths.input-field:gt(2)").show();
        for (let i=1; i < 11; i++) {
            row.find("div:nth-child("+i+")").find("label").text("Wert " + i);
        }
        for (let i=11; i < 21; i++) {
            row.find("div:nth-child("+i+")").find("label").text("Wert " + (i-10) + " (de)");
        }
        row.find("div:nth-child(3)").find("input").attr("placeholder", "optional");
    }
    }

});

$(document).ready(function () {
    var sel = $('select');
    sel.formSelect();
    sel.change();

    $(".clip > div.input-field > div.select-wrapper > input.select-dropdown").prop('disabled', true);
});

$(".clip-select").find("select").on('change', function () {
    if ($(this).val() === "No Clipping") {
        $(".clip > div.input-field > div.select-wrapper > input.select-dropdown").prop('disabled', true);
    } else {
        $(".clip > div.input-field > div.select-wrapper > input.select-dropdown").prop('disabled', false);
    }
    $(this).prop("selected", true);
});
