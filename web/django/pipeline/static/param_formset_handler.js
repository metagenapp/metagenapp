function updateElementIndex(el, prefix, ndx) {
    var id_regex = new RegExp('(' + prefix + '-\\d+)');
    var replacement = prefix + '-' + ndx;
    if ($(el).attr("for")) $(el).attr("for", $(el).attr("for").replace(id_regex, replacement));
    if (el.id) el.id = el.id.replace(id_regex, replacement);
    if (el.name) el.name = el.name.replace(id_regex, replacement);
}

function cloneMore(selector, prefix) {
    console.log(selector);
    console.log(prefix);
    var newElement = $(selector).clone(true);
    var total = $('#id_' + prefix + '-TOTAL_FORMS').val();
    console.log("clone: " + total);
    newElement.find(':input').each(function () {
        var name = $(this).attr('name');
        if (name) {
            name = name.replace('-' + (total - 1) + '-', '-' + total + '-');
            var id = 'id_' + name;
            console.log("ID:" + id);
            $(this).attr({'name': name, 'id': id}).val('').removeAttr('checked');
        }
    });
    total++;
    $('#id_' + prefix + '-TOTAL_FORMS').val(total);
    $(selector).after(newElement);
    var conditionRow = $('.form-row:not(:last)');
    conditionRow.find('.btn.add-form-row')
        .removeClass('btn-success').addClass('btn-danger')
        .removeClass('add-form-row').addClass('remove-form-row')
        .html('-');
    return false;
}

function deleteForm(prefix, btn) {
    console.log(prefix);
    var total = parseInt($('#id_' + prefix + '-TOTAL_FORMS').val());
    console.log(total);
    if (total > 1) {
        btn.closest('.form-row').remove();
        var forms = $('.form-row');
        $('#id_' + prefix + '-TOTAL_FORMS').val(forms.length);
        for (var i = 0, formCount = forms.length; i < formCount; i++) {
            $(forms.get(i)).find(':input').each(function () {
                updateElementIndex(this, prefix, i);
            });
        }
    }
    return false;
}

$(document).on('click', '.add-form-row', function (e) {
    e.preventDefault();
    cloneMore('.form-row:last', 'form');
    return false;
});
$(document).on('click', '.remove-form-row', function (e) {
    e.preventDefault();
    deleteForm('form', $(this));
    return false;
});

$(document).ready(function () {
    $("div.clip").children().prop('disabled', true)
});

$("#id_illumina_clipping").on("change", function () {
    if ($(this).val() === "No Clipping") {
        $("div.clip").children().prop('disabled', true)
    } else {
        $("div.clip").children().prop('disabled', false)
    }
});
