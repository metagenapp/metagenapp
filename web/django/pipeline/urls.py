from django.urls import path

from . import views

app_name = "pipeline"
urlpatterns = [
    path("config/", views.pipeline_config, name="config"),
    path("config/<uuid:run_id>/", views.pipeline_param_config, name="pconfig"),
    path("config/<uuid:run_id>/assign/", views.pipeline_param_assign, name="passign"),
    path("logs/", views.pipeline_logs, name="logs"),
    path("logs/<uuid:run_id>/", views.pipeline_logs_samples, name="run_logs"),
    path("logs/<uuid:run_id>/steps/", views.pipeline_logs_steps, name="run_logs_steps"),
    path("upload/", views.pipeline_upload, name="upload"),
    path("edit/", views.pipeline_edit, name="edit"),
    path("edit/edit/<uuid:run_id>", views.pipeline_run_edit, name="run_edit"),
    path("edit/delete/<uuid:run_id>", views.pipeline_delete, name="delete"),
]
