import uuid

from django.core.validators import MinValueValidator, MaxValueValidator
from django.utils.translation import gettext_lazy as _
from django.db import models

from results.models import PipelineRun, Sample, SampleParameter, SampleParameterPreset


class PipelineTrim(models.Model):
    class Meta:
        verbose_name = _("trimming parameter")
        verbose_name_plural = _("trimming parameters")

    id = models.UUIDField(primary_key=True,
                          default=uuid.uuid4,
                          editable=False)
    run = models.ForeignKey(PipelineRun,
                            on_delete=models.CASCADE,
                            default=0)
    leading_quality = models.IntegerField(default=20,
                                          choices=[(x, x) for x in range(1, 31)],
                                          validators=[MinValueValidator(0), MaxValueValidator(30)])
    trailing_quality = models.IntegerField(default=20,
                                           choices=[(x, x) for x in range(1, 31)],
                                           validators=[MinValueValidator(0), MaxValueValidator(30)])
    minimum_length = models.IntegerField(default=70,
                                         choices=[(x, x) for x in range(50, 310, 10)],
                                         validators=[MinValueValidator(50), MaxValueValidator(300)])
    sliding_window = models.IntegerField(default=4,
                                         choices=[(x, x) for x in range(1, 11)],
                                         validators=[MinValueValidator(1), MaxValueValidator(10)])
    sliding_window_quality = models.IntegerField(default=20,
                                                 choices=[(x, x) for x in range(1, 61)],
                                                 validators=[MinValueValidator(10), MaxValueValidator(60)])
    illumina_clipping = models.CharField(
        choices=[(x, x) for x in [_("No Clipping"), "TruSeq2-PE", "TruSeq3-PE-2", "TruSeq3-PE", "NexteraPE-PE"]],
        default=_("No Clipping"),
        max_length=13)

    crop = models.IntegerField(default=0,
                               choices=[(x, x) for x in range(0, 11)],
                               validators=[MinValueValidator(0), MaxValueValidator(10)])
    head_crop = models.IntegerField(default=0,
                                    choices=[(x, x) for x in range(0, 11)],
                                    validators=[MinValueValidator(0), MaxValueValidator(10)])


class PipelineTrimClip(models.Model):
    class Meta:
        verbose_name = _("clipping parameter")
        verbose_name_plural = _("clipping parameters")

    id = models.UUIDField(primary_key=True,
                          default=uuid.uuid4,
                          editable=False)
    run = models.ForeignKey(PipelineRun,
                            on_delete=models.CASCADE,
                            default=0)
    seed_mismatches = models.IntegerField(default=2,
                                          choices=[(x, x) for x in range(1, 101)],
                                          validators=[MinValueValidator(1), MaxValueValidator(100)])
    palindrome_clip_theshold = models.IntegerField(default=20,
                                                   choices=[(x, x) for x in range(10, 101)],
                                                   validators=[MinValueValidator(10), MaxValueValidator(100)])
    simple_clipping_theshold = models.IntegerField(default=30,
                                                   choices=[(x, x) for x in range(10, 101)],
                                                   validators=[MinValueValidator(10), MaxValueValidator(100)])
    # default 0 for no minAdapterLength
    minimum_adapter_length = models.IntegerField(default=0,
                                                 choices=[(x, x) for x in range(0, 31)],
                                                 validators=[MinValueValidator(0), MaxValueValidator(30)])


class PipelineLog(models.Model):
    class Meta:
        verbose_name = _("log message")
        verbose_name_plural = _("log messages")

    id = models.UUIDField(primary_key=True,
                          default=uuid.uuid4,
                          editable=False)
    run = models.ForeignKey(PipelineRun,
                            on_delete=models.CASCADE)
    sample = models.ForeignKey(Sample,
                               on_delete=models.CASCADE,
                               blank=True,
                               null=True)
    level = models.CharField("level", max_length=10)
    tool = models.CharField(_("tool"),
                            max_length=50)
    message = models.TextField(_("message"))
    timestamp = models.DateTimeField(_("timestamp"))
