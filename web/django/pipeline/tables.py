import django_tables2 as tables
from django.utils.html import mark_safe, format_html
from django.utils.translation import gettext_lazy as _

from .models import PipelineRun


class MaterializeCssCheckboxColumn(tables.TemplateColumn):
    def render(self, value, bound_column, record):
        default = {"type": "checkbox", "name": bound_column.name, "value": value}
        if self.is_checked(value, record):
            default.update({"checked": "checked"})

        general = self.attrs.get("input")
        specific = self.attrs.get("td__input")
        attrs = tables.utils.AttributeDict(default, **(specific or general or {}))
        return mark_safe("<p><label><input %s/><span></span></label></p>" % attrs.as_html())


class ImageColumn(tables.Column):
    def render(self, value):
        return format_html('<img src="{url}" height="60px", width="90px">', url=value.url)


class EllipsisColumn(tables.Column):
    def render(self, value):
        return format_html('<div class="ellipsis">{text}</div>', text=value)


class RunTable(tables.Table):
    image = ImageColumn()
    analysis_date = tables.DateTimeColumn(format='d.m.y - G:i:s')
    edit = tables.TemplateColumn(verbose_name=_("Edit"),
                                 attrs={"th": {"style": "text-align:center!important"},
                                        "td": {"style": "text-align:center!important"}},
                                 template_code='<button class="btn table-btn" type="submit">'
                                               '<div class="valign-wrapper">'
                                               '<i class="material-icons white-text">edit</i>'
                                               '</div/button>',
                                 linkify=("pipeline:run_edit", (tables.A("pk"),)))
    delete = tables.TemplateColumn(verbose_name=_("Delete"),
                                   attrs={"th": {"style": "text-align:center!important"},
                                          "td": {"style": "text-align:center!important"}},
                                   template_code='<button class="btn table-btn" type="submit">'
                                                 '<div class="valign-wrapper">'
                                                 '<i class="material-icons white-text">delete_forever</i>'
                                                 '</div/button>',
                                   linkify=("pipeline:delete", (tables.A("pk"),)))
    description_en = EllipsisColumn(attrs={"th": {"class": "trunc-col"},
                                        "td": {"class": "trunc-col"}})
    description_de = EllipsisColumn(attrs={"th": {"class": "trunc-col"},
                                           "td": {"class": "trunc-col"}})
    # details_en = EllipsisColumn(attrs={"th": {"class": "trunc-col"},
    #                                 "td": {"class": "trunc-col"}})
    # details_de = EllipsisColumn(attrs={"th": {"class": "trunc-col"},
    #                                 "td": {"class": "trunc-col"}})

    class Meta:
        model = PipelineRun
        fields = ("image", 'analysis_en', 'mode', 'analysis_date', "status",
                  "description_en", "description_de")
