import os
from xml.etree import ElementTree
from zipfile import ZipFile

from django.core import serializers, exceptions
from django.shortcuts import render, get_object_or_404, redirect
from django_tables2 import RequestConfig

from app.session_controller import SessionController, redirect_run, update_session, no_sidnav_fix, update_steps
from .forms import UploadMenu, ConfigForm, TrimForm, TrimClipForm, SampleFormSet, SampleMenu, StepsMenu, \
    SampleParameterPresetFormSet, SampleParameterFormSet, RunForm
from .models import PipelineRun, Sample, PipelineLog, SampleParameter
from .tables import RunTable


@no_sidnav_fix
def pipeline_config(request):
    template_name = 'pipeline/config.html'

    if request.method == 'GET':
        run_form = ConfigForm(request.GET or None)
        trim_form = TrimForm(request.GET or None)
        trim_clip_form = TrimClipForm(request.GET or None)
        sample_form_set = SampleFormSet(request.GET or None, queryset=Sample.objects.none(), )

        return render(request, template_name, {
            "form": run_form,
            "trim_form": trim_form,
            "trim_clip_form": trim_clip_form,
            'formset': sample_form_set,
        })

    elif request.method == 'POST':
        run_form = ConfigForm(request.POST, request.FILES)
        trim_form = TrimForm(request.POST)
        trim_clip_form = TrimClipForm(request.POST)
        sample_form_set = SampleFormSet(request.POST, request.FILES)
        if run_form.is_valid() \
                and trim_form.is_valid() \
                and sample_form_set.is_valid():
            run = run_form.save()
            for sf in sample_form_set:
                sample = sf.save(commit=False)
                sample.run = run
                sample.save()

            trim = trim_form.save(commit=False)
            trim.run = run
            trim.save()
            if trim.illumina_clipping != "No Clipping" and trim_clip_form.is_valid():
                trim_clip = trim_clip_form.save(commit=False)
                trim_clip.run = run
                trim_clip.save()

            return redirect(f"{run.id}/")
        else:
            return render(request, template_name, {
                "form": run_form,
                "trim_form": trim_form,
                "trim_clip_form": trim_clip_form,
                'formset': sample_form_set,
            })


@no_sidnav_fix
def pipeline_param_config(request, run_id):
    template_name = 'pipeline/pconfig.html'
    active_run = get_object_or_404(PipelineRun, pk=run_id)

    if request.method == 'GET':
        formset = SampleParameterPresetFormSet(request.GET or None,)

        return render(request, template_name, {'formset': formset})

    elif request.method == 'POST':
        formset = SampleParameterPresetFormSet(request.POST)
        if formset.is_valid():
            for f in formset:
                param = f.save(commit=False)
                param.run = active_run
                param.save()

            return redirect(f"assign/")

        else:
            return render(request, template_name, {'formset': formset})


@no_sidnav_fix
def pipeline_param_assign(request, run_id):
    template_name = 'pipeline/passign.html'
    samples = Sample.objects.filter(run_id=run_id)

    if request.method == 'GET':
        formsets = [SampleParameterFormSet(form_kwargs={"run_id": run_id,
                                                        "sample_id": s.id},
                                           prefix=f"{s.name}: ") for s in samples]

        return render(request, template_name, {'formsets': formsets})

    elif request.method == 'POST':
        run = PipelineRun.objects.get(pk=run_id)
        for sample in samples:
            fieldstart = f"{sample.name}: -0-"
            params = {}
            for k, v in request.POST.items():
                if k.startswith(fieldstart + "sample_id"):
                    pass
                elif k.startswith(fieldstart):
                    params[k[len(fieldstart):]] = [v, request.POST[k + "_dt"]]
            for param, value in params.items():
                dt = value[1]
                order = int(value[0][-1]) if dt == "ordinal" else None
                val = value[0][:-1]

                SampleParameter.objects.create(sample_id=sample.id,
                                               name_en=param,
                                               datatype=dt,
                                               run=run,
                                               value=val,
                                               order=order)

        run.status = "started"
        run.save()

        return render(request, "pipeline/logs_base.html", {
            "runs": PipelineRun.objects.all(),
        })


@redirect_run
def pipeline_logs(request):
    template_name = "pipeline/logs_base.html"
    session = SessionController(request)

    if session.active_run:
        redirect("pipeline:run_logs", run_id=session.active_run)

    return render(request, template_name, {
        "runs": PipelineRun.objects.all(),
    })


@update_session
def pipeline_logs_samples(request, run_id):
    template_name = "pipeline/logs_run.html"
    samples = Sample.objects.filter(run_id=run_id)
    session = SessionController(request)
    steps = PipelineLog.objects.filter(run_id=run_id).order_by("timestamp").only("id", "tool")
    steps_form = StepsMenu(qs=steps, prefix="steps_form", session=session)
    logs = PipelineLog.objects.filter(sample_id__in=session.active_samples,
                                      run_id=run_id,
                                      tool__in=session.active_logs)

    if request.method == "POST":
        samples_form = SampleMenu(request.POST, qs=samples, prefix="samples_form", session=session)

    else:
        samples_form = SampleMenu(qs=samples, prefix="samples_form", session=session)

    return render(request, template_name, {
        "runs": PipelineRun.objects.all(),
        "samples_form": samples_form,
        "steps_form": steps_form,
        "logs": logs,
    })


@update_steps
def pipeline_logs_steps(request, run_id):
    session = SessionController(request)

    return redirect("pipeline:run_logs", run_id=session.active_run)


@no_sidnav_fix
def pipeline_upload(request):
    template_name = "pipeline/upload.html"
    message = None

    def handle_uploaded_file(file):
        with open("/var/www/input/upload.zip", "wb+") as dest:
            for chunk in file.chunks():
                dest.write(chunk)
        with ZipFile("input/upload.zip", "r") as zip_fh:
            for file in zip_fh.namelist():
                if file.endswith(".xml"):
                    xml_file = zip_fh.open(file)
                    try:
                        root = ElementTree.fromstring(xml_file.read())
                        pk = root.find("object[@model='results.pipelinerun']").attrib['pk']
                        try:
                            PipelineRun.objects.get(pk=pk)

                            return "Run already in database. Upload aborted!"

                        except exceptions.ObjectDoesNotExist:
                            pass

                        xml_file.seek(0)
                        for obj in serializers.deserialize("xml", xml_file):
                            obj.save()

                        image = PipelineRun.objects.get(pk=pk).image
                        print(image.name)
                        from pathlib import Path
                        print(Path.cwd())
                        zip_fh.extract(image.name, f"media")

                        return 'Run successfully uploaded!'

                    except (ElementTree.ParseError, UnicodeDecodeError) as e:
                        print(e)
                        return "Error parsing xml file!"

            os.remove("input/upload.zip")

    if request.method == 'POST':
        upload_form = UploadMenu(request.POST, request.FILES)
        if upload_form.is_valid():
            xml_file = request.FILES['xml_file']
            message = handle_uploaded_file(xml_file)

    else:
        upload_form = UploadMenu()

    return render(request, template_name, {"upload_form": upload_form, "message": message})


@no_sidnav_fix
def pipeline_edit(request):
    template_name = "pipeline/edit.html"

    run_table = RunTable(PipelineRun.objects.all(), order_by="analysis_en")

    RequestConfig(request).configure(run_table)

    return render(request, template_name, {"run_table": run_table})


@no_sidnav_fix
def pipeline_run_edit(request, run_id):
    template_name = "pipeline/run_edit.html"
    run = PipelineRun.objects.get(pk=run_id)
    print(run.image.url)

    if request.method == "POST":
        run_form = RunForm(request.POST, request.FILES, instance=run)
        if run_form.is_valid():
            run_form.save()

            return redirect("pipeline:edit")

        else:
            pass

    else:
        run_form = RunForm(instance=run)

        return render(request, template_name, {"run_form": run_form, "image": run.image})


@no_sidnav_fix
def pipeline_delete(request, run_id):

    run = PipelineRun.objects.get(pk=run_id)
    run.delete()
    run_table = RunTable(PipelineRun.objects.all(), order_by="analysis_en")
    RequestConfig(request).configure(run_table)

    return redirect("pipeline:edit")
