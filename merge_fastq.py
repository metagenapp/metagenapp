import os
import subprocess


path = "/group/projects/Palm/rawdata/palm_swab_201904/fastq/"

for path, dirs, files in os.walk(path):
    for file in files:
        prefix = "H7MFMDRXX_1"
        if file.startswith(prefix):
            file1 = path + file
            file2 = path + "H7MFMDRXX_2" + file[len(prefix):]
            outfile = path + file.split("_")[1][2:] + "_" + file[-7] + ".fq.gz"
            cmd = "cat " + file1 + " " + file2 + " > " + outfile
            print(cmd)
            subprocess.call(cmd)
