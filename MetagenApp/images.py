import binascii
import gzip
import os
import platform
import shutil
import tarfile
import time
import socket
from abc import ABC
from pathlib import Path

import docker
import docker.models.containers
import wget
from docker.errors import ImageNotFound
from pathos.multiprocessing import ThreadPool
from urlpath import URL
from psycopg2 import OperationalError
from collections import namedtuple

import MetagenApp.config as config
from MetagenApp.containers import WebContainer
from MetagenApp.logger import setup_logger, close_handles
from functools import partial
from contextlib import closing
from urllib.error import HTTPError


class BaseImage(ABC):
    def __init__(self, dockerclient, image=None, dockerfile=None, map_uid=True, **kwargs):
        self._image = image if image else "metagenapp/" + str(dockerfile).split("/")[-1] + ":latest"
        self._image_name = self._image.split("/")[-1].split(":")[0]
        self._tag = self._image.split(":")[1] if ":" in self._image else "latest"
        self._image_on_client = False
        self._dockerfile = dockerfile
        self._volumes = {}
        self._client = dockerclient
        self._default_kwargs = {"network": "metagen_net"}
        self._map_uid = map_uid
        self.check_image()

    @property
    def default_kwargs(self):
        other_kwargs = {"volumes": self.volumes}
        if platform.system() != "Windows" and (self._map_uid == True):
            other_kwargs["user"] = os.getuid()
        return {**self._default_kwargs, **other_kwargs}

    def add_default_kwarg(self, key, value):
        self._default_kwargs[key] = value

    @property
    def image(self):
        return self._image

    @property
    def image_name(self):
        return self._image_name

    @property
    def tag(self):
        return self._tag

    @property
    def dockerfile(self):
        return self._dockerfile

    @property
    def volumes(self):
        return self._volumes

    @property
    def client(self):
        return self._client

    def add_volume(self, host_dir, container_dir, mode="rw"):
        for k, v in self._volumes.items():
            if container_dir == v:
                del self._volumes[k]

        self._volumes[host_dir] = {"bind": container_dir, "mode": mode}

    def check_image(self):
        print(f"Looking for image {self._image} on client...", end="")
        try:
            self._client.images.get(self._image)
            self._image_on_client = True
            print("done!")

        except ImageNotFound:
            print("not found!")
            if self._dockerfile:
                self.build_image()

            else:
                self.pull_image()

    def check_port(self, port):
        """
        Test if port is in use!
        :param port: port number (int)
        :return: bool
        """
        with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as s:
            return s.connect_ex(('localhost', port)) == 0

    def find_port(self, port):
        """
        Find next free port
        :param port:
        :return:
        """
        if self.check_port(port):
            print(f"Port {port} in use!")
            port += 1
            self.find_port(port)

        return port

    def build_image(self):
        print("Building image from dockerfile...", end="")
        try:
            dockerfiledir = str(Path(os.path.dirname(__file__)) / self._dockerfile)
            self.client.images.build(path=dockerfiledir, tag=self._image, rm=True)
            self._image_on_client = True
            print("done!")
        except docker.errors.BuildError:
            print("Build failed!")
            exit()

    def pull_image(self):
        print(f"Try pulling {self._image} ...", end="")
        try:
            self.client.images.pull(self.image.split(":")[0], tag=self._tag)
            self._image_on_client = True
            print("done!")
        except docker.errors.NotFound:
            print("Pull failed!")
            exit()


class BaseWebImage(BaseImage):
    def __init__(self, dockerclient, image, dockerfile, map_uid=True):
        super().__init__(dockerclient, image, dockerfile, map_uid=map_uid)
        self.add_volume(config.input_directory, f"{config.container_django_dir}input/")
        self.add_volume(Path.cwd() / "web" / "django", config.container_django_dir)
        self.add_volume(config.data_directory, "/db/")
        self.add_default_kwarg("detach", True)
        self.add_default_kwarg("restart_policy", {"Name": "on-failure"})

    def runserver(self):
        """
        starts container with django development server running in detached mode
        :return: WebContainer instance
        """
        host_port = self.find_port(8000)
        print("Starting container metagen_web_1")
        container = self.client.containers.run(self.image,
                                               name="metagen_web_1",
                                               working_dir=config.container_django_dir,
                                               ports={host_port: 8000},
                                               command="python manage.py runserver 0.0.0.0:8000",
                                               environment={"PG_PORT": config.pg_port},
                                               **self.default_kwargs)

        return WebContainer(container)


class WebImage(BaseWebImage):
    def __init__(self, dockerclient):
        super().__init__(dockerclient, image="metagen_web:latest",
                         dockerfile=Path.cwd() / "web" / "web_container")
        self.add_default_kwarg("stream", True)
        self.add_default_kwarg("tty", True)


class WebImageDev(BaseWebImage):
    def __init__(self,dockerclient, map_uid):
        super().__init__(dockerclient, image="metagen_web_dev:latest",
                         dockerfile=Path.cwd() / "web" / "web_container_dev",
                         map_uid=map_uid)
        self.add_volume("/var/run/docker.sock", "/var/run/docker.sock")
        self.add_default_kwarg("stream", True)
        self.add_default_kwarg("tty", True)

    def run_notebook(self):
        """
        starts container with django jupyter server
        :return: WebContainer instance
        """
        host_data_directory = Path.cwd() / "web" / "django" / "notebooks/data"
        host_port = self.find_port(8888)
        container = self.client.containers.run(self.image,
                                               name="metagen_web_1",
                                               environment={"PG_PORT": config.pg_port,
                                                            "HOSTPATH": host_data_directory},
                                               entrypoint="/entrypoint_nb.sh",
                                               privileged=True,
                                               working_dir=f"{config.container_django_dir}notebooks/",
                                               ports={host_port: 8888},
                                               command="python ../manage.py shell_plus --notebook",
                                               **self.default_kwargs)

        return WebContainer(container)


class WebImageDevNginx(BaseWebImage):
    def __init__(self, dockerclient, map_uid=False):
        super().__init__(dockerclient, image="metagen_ngnix:latest",
                         dockerfile=Path.cwd() / "web" / "web_container_nginx",
                         map_uid=map_uid)
        self.add_volume(Path.cwd() / "web" / "django" / "notebooks" / "data", "/usr/share/nginx/html", mode="ro")

    def runserver(self):
        """
        starts container with ngnix providing static files for notebook
        :return: WebContainer instance
        """
        print("Starting container metagen_web_ngnix")
        hostport = self.find_port(80)
        self.client.containers.run(self._image,
                                   name="metagen_nginx",
                                   ports={hostport: 8887},
                                   command="nginx-debug -g 'daemon off;'",
                                   **self.default_kwargs)


class WebImageNginx(BaseWebImage):
    def __init__(self, dockerclient, map_uid=False):
        super().__init__(dockerclient, image="metagen_ngnix:latest",
                         dockerfile=Path.cwd() / "web" / "web_container_nginx",
                         map_uid=map_uid)
        self.add_volume(Path.cwd() / "web" / "django" / "static", "/usr/share/nginx/html", mode="ro")

    def runserver(self):
        """
        starts container with ngnix providing static files for notebook
        :return: WebContainer instance
        """
        print("Starting container metagen_web_ngnix")
        config.ngnix_port = self.find_port(80)
        self.client.containers.run(self._image,
                                   name="metagen_nginx",
                                   ports={config.ngnix_port: config.ngnix_port},
                                   # command="nginx-debug -g 'daemon off;'",
                                   **self.default_kwargs)


class PipelineImage(BaseImage):
    def __init__(self, dockerclient, image=None, dockerfile=None, map_uid=True,
                 output_dir=None, db_host=None, db_container=None, db_url="", db_rename="", data_loader=None):
        super().__init__(dockerclient, image=image, dockerfile=dockerfile, map_uid=map_uid)
        self._host_dir = Path(Path.cwd() / "web/django/input")
        self._volume_dir = Path.cwd()
        self._output_dir = output_dir if output_dir else self._image_name
        self._log_dir = Path.cwd() / f"MetagenApp/logs/{config.analysis}/{self._image_name}"
        self._log_dir.mkdir(exist_ok=True, parents=True)
        if db_host:
            self.add_database(db_host, db_container, url=db_url, rename=db_rename, data_loader=data_loader)

        self.add_default_kwarg("cap_add", "SYS_NICE")
        self.add_default_kwarg("working_dir", "/data/")
        self.add_default_kwarg("stdout", True)
        self.add_default_kwarg("stderr", True)
        self.add_default_kwarg("links", {"db": "db"})

    @property
    def output_dir(self):
        return self._output_dir

    @property
    def volume_dir(self):
        return self._volume_dir

    @property
    def host_dir(self):
        return self._host_dir

    @property
    def log_dir(self):
        return self._log_dir

    def create_volume_directory(self, sample):
        """
        Create sub directories in the input volumes
        :param sample: Sample object
        :return: None
        """
        p = Path(sample.host_dir) / self.output_dir / sample.name
        p.mkdir(exist_ok=True, parents=True)

    def is_gz_file(self, filepath):
        """
        Check if file is gzipped
        :param filepath: path object
        :return: bool
        """
        with open(filepath, 'rb') as test_f:
            return binascii.hexlify(test_f.read(2)) == b'1f8b'

    def download_db(self, url, rename=""):
        """
        Download and rename database/file
        :param url:
        :param rename:
        :return:
        """
        data_dir = Path.cwd() / "MetagenApp/data"
        download_file = data_dir / url.name
        db_dir = data_dir / url.stem
        renamed_dir = data_dir / rename
        print(f"downlading {url.stem}")
        try:
            wget.download(str(url), bar=wget.bar_adaptive, out=str(data_dir))
        except HTTPError:
            print(f"URL: {url} is not availiable\nMaybe the homepage is down or the URL has changed.\nexiting...")
            exit()

        print(os.linesep)
        if rename != "":
            new_name = data_dir.joinpath(rename + "".join(url.suffixes))
            download_file.rename(new_name)
            download_file = new_name
            db_dir = renamed_dir

        if tarfile.is_tarfile(str(download_file)):
            print(f"Extracting {download_file} ...", end="")
            with tarfile.open(str(download_file)) as fh:
                fh.extractall(path=db_dir)
            print("done!")
            os.remove(download_file)

        elif self.is_gz_file(str(download_file)):
            print(f"Unpacking {download_file} ...", end="")
            with gzip.open(str(download_file), 'rb') as f_in:
                path = db_dir.joinpath(rename + "".join(url.suffixes))
                os.makedirs(str(db_dir), exist_ok=True)
                with open(str(path)[:-3], 'wb') as f_out:
                    shutil.copyfileobj(f_in, f_out)
            print("done!")
            os.remove(download_file)

        else:
            os.makedirs(str(renamed_dir), exist_ok=True)
            os.rename(str(download_file), str(renamed_dir.joinpath(rename + url.suffix)))

    def add_database(self, host_dir, container_dir, url="", rename="", data_loader=None):
        """
        call donload_db and/or call data_loader and add directory to container volumes
        :param host_dir: path object, host path to mount
        :param container_dir: str container volume name
        :param url: download url
        :param rename: str
        :param data_loader: data loader function
        :return: None
        """
        host_dir = Path(host_dir)
        url = URL(url)
        db_dir = rename if rename != "" else url.stem
        if host_dir == Path.cwd() / "MetagenApp/data" / db_dir:  # default db path
            if not host_dir.is_dir():
                if str(url) != "":
                    self.download_db(url, rename=rename)
                else:
                    host_dir.mkdir()

                if data_loader:
                    data_loader()

                else:
                    config.main_logger.info(f"Database: {db_dir} found!")
        self.add_volume(host_dir, container_dir, mode="ro")

    def remove_containers(self):
        filters = {"ancestor": self.image}
        containerlist = self.client.containers.list(all=True, filters=filters)
        [c.remove() for c in containerlist]

    def run_single(self, cmd_func, filepath, *args):
        logger_name = "_".join([config.analysis, self.image_name, "all"])
        logger_file = self.log_dir / "all.log"
        logger = setup_logger(logger_name, logger_file)
        name = "_".join(["metagen", self.image_name])
        self.volumes.update({Path(filepath).parents[0]: {"bind": "/data/", "mode": "rw"}})

        log = self.client.containers.run(self.image, name=name, remove=True,
                                    command=cmd_func(filepath, *args), **self.default_kwargs)

        Sample = namedtuple("sample", ["name"])
        self.log_run(log, cmd_func, logger, Sample("all"), name)

        del self.volumes[Path(filepath).parents[0]]

    def _run(self, sample, cmd_func, **kwargs):
        """
        Start biocontainer
        :param sample: sample object
        :param cmd_func: function returning command string
        :return: None
        """
        logger_name = "_".join([config.analysis, self.image_name, sample.name])
        fn = sample.name + ".log"
        logger_file = self.log_dir / fn
        logger = setup_logger(logger_name, logger_file)

        name = "_".join(["metagen", self.image_name, sample.name])
        self.volumes.update({Path(sample.host_dir): {"bind": "/data/", "mode": "rw"}})
        command = f'nice -n 5 bash -c "{cmd_func(sample, **kwargs)}"'

        log = self.client.containers.run(self.image, name=name, command=command, **self.default_kwargs)
        self.log_run(log, cmd_func.func, logger, sample, name)
        close_handles(logger)

    def log_run(self, log, cmd_func, logger, sample, name):
        """5432
        write log msg to logfile and to database
        :param log:
        :param cmd_func:
        :param logger:
        :param sample:
        :param name:
        :return:
        """
        if log.decode(encoding="utf-8") == "":
            log = "exit 0"
        else:
            log = log.decode(encoding="utf-8")

        if hasattr(cmd_func, "log_name"):
            res = self.write_log_to_db(cmd_func, log, sample.name, logger)
            if not res:
                logger.error("Writing log to database failed!",
                             extra={'name_override': name})

            logger.info(log, extra={'name_override': cmd_func.log_name + "-" + sample.name})
            close_handles(logger)

        else:
            try:
                print(f"{name}: {log}")
            except AttributeError:
                pass

    def write_log_to_db(self, cmd_func, log, sample_name, logger):
        """
        try to add log string to database
        :param cmd_func: function with log_name attribute
        :param log: log message
        :param sample_name: string
        :param container_name: string
        :param logger: logger instance
        :return: bool (sucsess or not)
        """
        cnt = 0
        while cnt < 10:
            cnt += 1
            try:
                webcontainer = WebContainer(self.client.containers.get("metagen_web_1"))
                webcontainer.add_log("info", cmd_func.log_name, sample_name, log)
                return True
            except OperationalError as e:
                logger.warn(str(e) + "\n retry:")
                time.sleep(2)

    def run(self, command_func, samples, pool_size, **kwargs):
        """
        Start threading pool starting biocontainers
        :param command_func: mapped function
        :param samples: sample list
        :param pool_size: int
        :return:
        """
        config.main_logger.info(f"Starting {self.image_name}")
        for sample in samples:
            self.create_volume_directory(sample)
        pool = ThreadPool(pool_size)

        try:
            pool.map(lambda x: self._run(x, partial(command_func, **kwargs)), samples)
            pool.close()
            pool.join()
        except KeyboardInterrupt:
            print("Terminate Threadpool processes...")
            pool.terminate()
            print("Threadpool closed.")
            exit()

        self.remove_containers()
        config.main_logger.info(f"{self.image_name} finished")


class DbImage(BaseImage):
    def __init__(self, dockerclient, name, map_uid=True):
        super().__init__(dockerclient, image="postgres", dockerfile=None, map_uid=map_uid)
        self._name = name
        if platform.system() != "Windows":
            self.add_volume(config.postgres_volume_directory, "/var/lib/postgresql/data")
        self._postgres_user = "postgres"
        self._db_name = "postgres"
        self._postgres_password = "docker"

    def run(self):
        print(f"Starting PostgresDB with volume: {config.postgres_volume_directory}")
        hostport = self.find_port(5432)
        config.set("pg_port", hostport)
        self._client.containers.run(self.image,
                                    name=self._name,
                                    detach=True,
                                    hostname="metagen_db",
                                    environment={
                                        "POSTGRES_PASSWORD": self._postgres_password,
                                        "POSTGRES_USER": self._postgres_user,
                                        "POSTGRES_DB": self._db_name
                                    },
                                    command=f"postgres -p {config.pg_port}",
                                    ports={config.pg_port: config.pg_port},
                                    restart_policy={"Name": "on-failure"},
                                    **self.default_kwargs
                                    )
