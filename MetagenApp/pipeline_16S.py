import os
import shutil
from pathlib import Path

from MetagenApp import config
from MetagenApp.decorators import log_name
from MetagenApp.images import PipelineImage
from MetagenApp.data_loaders import silva_loader
from MetagenApp.samplelist import Samples


def make_manifest(samples):
    for sample in samples:
        out_dir = config.input_directory/"manifest"/sample.name
        out_dir.mkdir(parents=True)
        with open(f"{out_dir}/{sample.name}_manifest.csv", "w") as manifest:
            header = "sample-id,absolute-filepath,direction\n"
            manifest.write(header)
            manifest.write(f"{sample.name},{sample.fwd_path},forward\n")
            manifest.write(f"{sample.name},{sample.rev_path},reverse\n")


@log_name("krona")
def krona_cmd(sample):
    return f"ktImportText -q -o /data/krona/{sample.name}/{sample.name}_kronaplot.html {sample.path}"


@log_name("import_fastq")
def import_data(sample):
    out = f"/data/core/{sample.name}/{sample.name}_fastq.qza"
    return f"qiime tools import --type 'SampleData[PairedEndSequencesWithQuality]' --input-path {sample.path}  " \
        f"--output-path {out} --input-format PairedEndFastqManifestPhred33"


@log_name("denoise")
def denoise(sample):
    out = f"/data/dada2/{sample.name}"
    return f"qiime dada2 denoise-paired  --i-demultiplexed-seqs {sample.path}  --p-trunc-len-f 0  --p-trunc-len-r 0 " \
        f"--p-n-threads {config.max_threads}  --output-dir {out}"


@log_name("silva-classfication")
def classify(sample):
    ref = "/db/silva132_99.qza"
    tax = "/db/taxonomy_7_levels.qza"
    out = f"/data/qiime_silva/{sample.name}"
    return f"qiime feature-classifier classify-consensus-vsearch --i-query {sample.path} " \
        f"--i-reference-reads {ref} --i-reference-taxonomy {tax} --p-threads {config.max_cpu} --output-dir {out}"


def export_classification(sample):
    return f"qiime tools export --input-path {sample.path} --output-path /data/qiime_silva/{sample.name}/"


def make_krona_txt(samples):
    for sample in samples:
        outfile = open(config.input_directory/f"qiime_silva/{sample.name}/krona.txt", "w")
        with open(config.input_directory/f"qiime_silva/{sample.name}/taxonomy.tsv") as inflie:
            inflie.readline()
            for line in inflie.readlines():
                line = line.split("\t")
                taxonomy = line[1].split(";") if line[1] != "Unassigned" else ["     Unassigned"]
                outfile.write("1\t" + "\t".join([t[5:] for t in taxonomy if t != "uncultured bacterium"]) + "\n")


def run_16S(web_container, dockerclient):
    trim_merged = Samples.collect("bash", ".*\_trim_merged.fq", pe=True)
    trim_merged.remove_dir()
    qiime = PipelineImage(dockerclient, image="quay.io/biocontainers/qiime:1.9.1--np110py27_1", map_uid=False)
    qiime_config_path = Path(os.path.dirname(__file__)) / "containers/qiime"
    qiime.add_volume(qiime_config_path, "/config/", mode="ro")
    qiime.add_database(config.silva_qiime, "/ref/")

    krona = PipelineImage(dockerclient, image="quay.io/biocontainers/krona:2.7--pl526_2")
    qiime2 = PipelineImage(dockerclient, image="qiime2/core:2019.4",
                           db_host=config.silva_qiime,
                           db_container="/db/",
                           db_url="https://www.arb-silva.de/fileadmin/silva_databases/qiime/Silva_132_release.zip",
                           db_rename="silva_qiime", data_loader=silva_loader)

    trim = Samples.collect("trimmomatic", ".*P", pe=True)
    make_manifest(trim)
    manifest = Samples.collect("manifest", ".*_manifest.csv")

    qiime2.run(import_data, manifest, config.max_container)
    qiime2.add_database(config.silva_qiime, "/db/")

    qza = Samples.collect("core", ".*.qza")
    qiime2.run(denoise, qza, config.max_container)

    denoised = Samples.collect("dada2", "representative_sequences.qza")
    qiime2.run(classify, denoised, 1)

    classification = Samples.collect("qiime_silva", "classification.qza")
    qiime2.run(export_classification, classification, config.max_container)
    tab = Samples.collect("qiime_silva", "taxonomy.tsv")

    make_krona_txt(tab)

    krona_txt = Samples.collect("qiime_silva", "krona.txt")

    krona.run(krona_cmd, krona_txt, config.max_container)
    web_container.add_krona(config.samples)


    @log_name("beta")
    def qiime_beta(filepath, rank):
        return f"qiime diversity beta --p-metric braycurtis --i-table {filepath.name} --o-distance-matrix distance_{rank}.qza --p-n-jobs {config.max_cpu}"

    @log_name(f"qiime-pcoa")
    def qiime_pcoa(filepath, rank):
        return f"qiime diversity pcoa --i-distance-matrix {filepath.name} --o-pcoa pcoa_{rank}.qza"

    def qiime_rel_freq(filepath, rank):
        return f"qiime feature-table relative-frequency --i-table {filepath.name} --o-relative-frequency-table ab_rel_{rank}.qza"

    def qiime_biplot(filepath, rank):
        return f"qiime diversity pcoa-biplot --i-pcoa {filepath.name} --i-features ab_rel_{rank}.qza --o-biplot biplot_{rank}.qza --quiet"

    def qiime_emperor(filepath, rank):
        return f"qiime emperor plot --i-pcoa biplot_{rank}.qza --m-metadata-file map.txt --output-dir emperorplot_{rank} --quiet"

    def qiime_export_emperor(filepath, rank):
        return f"qiime tools export --input-path emperorplot_{rank}/visualization.qzv --output-path emperorplot_{rank}"

    def qiime_export_abundance(filepath, rank):
        return f"qiime tools export --input-path {filepath.name} --output-path abundance_{rank}"

    def collapse(sample):
        out = f"/data/abundance_{i}/{sample.name}/ab_{i}.qza"
        ft = f"/data/dada2/{sample.name}/table.qza"
        return f"qiime taxa collapse --i-table {ft} --i-taxonomy {sample.path} --o-collapsed-table {out} --p-level {i}"

    def export_abundance(sample):
        return f"qiime tools export --input-path {sample.path} --output-path /data/abundance_{i}/{sample.name}/"

    def biom2table(sample):
        out = f"/data/abundance_{i}/{sample.name}/ab_{i}.tsv"
        return f"biom convert -i {sample.path} -o {out} --table-type='OTU table' --to-tsv"
    if len(classification) > 3:
        for i in range(1, 8):
            [Path(config.input_directory).joinpath(f"abundance_{i}/{s.name}/").mkdir(parents=True, exist_ok=True) for s in classification]
            qiime2.run(collapse, classification, config.max_container)
            abundance = Samples.collect(f"abundance_{i}", f"ab_{i}.qza")
            qiime2.run(export_abundance, abundance, config.max_container)
            abundance = Samples.collect(f"abundance_{i}", f"feature-table.biom")
            qiime2.run(biom2table, abundance, config.max_container)

        web_container.get_metadata()
        tables_dir = config.input_directory.joinpath(f"qiime")

        for i, rank in enumerate(["P", "C", "O", "F", "G", "S"], start=2):
            @log_name("qiime2_merge")
            def merge_tables(filepath, samples):
                out = f"/data/qiime/ab_{rank}.qza"
                return f"qiime feature-table merge --i-tables {' --i-tables '.join([s.path for s in samples])}  --o-merged-table {out}"

            tables = Samples.collect(f"abundance_{i}", f"ab_{i}.qza")
            qiime2.run_single(merge_tables, tables_dir, tables)
            merged = tables_dir.joinpath(f"ab_{rank}.qza")
            qiime2.run_single(qiime_export_abundance, merged, rank)
            qiime2.run_single(qiime_beta, merged, rank)
            dist =tables_dir.joinpath(f"distance_{rank}.qza")
            qiime2.run_single(qiime_pcoa, dist, rank)
            pcoa = tables_dir.joinpath(f"pcoa_{rank}.qza")
            qiime2.run_single(qiime_rel_freq, merged, rank)
            qiime2.run_single(qiime_biplot, pcoa, rank)
            viz = tables_dir.joinpath(f"emperorplot_{rank}")
            qiime2.run_single(qiime_emperor, viz, rank)
            qiime2.run_single(qiime_export_emperor, viz, rank)

    web_container.add_taxonomy_16S(config.samples)

    shutil.rmtree(config.input_directory.joinpath("qiime_silva"))
    shutil.rmtree(config.input_directory.joinpath("manifest"))
    shutil.rmtree(config.input_directory.joinpath("dada2"))
    emperor_plots = config.input_directory.joinpath("qiime").glob("emperorplot_*")

    for src in emperor_plots:
        target = config.input_directory.joinpath(f"emperor_plots/{src.parts[-1]}")
        if target.is_dir():
            shutil.rmtree(target, ignore_errors=True)
        shutil.move(src, target)
    tables = config.input_directory.joinpath("qiime").glob("abundance_*")
    config.input_directory.joinpath(f"abundance_tables/").mkdir(exist_ok=True)
    [shutil.move(str(t), str(config.input_directory.joinpath(f"abundance_tables/{t.parts[-1]}"))) for t in tables
     if not config.input_directory.joinpath(f"abundance_tables/{t.parts[-1]}").is_dir()]
    shutil.rmtree(config.input_directory.joinpath("qiime"), ignore_errors=True)
    [shutil.rmtree(config.input_directory.joinpath(f"abundance_{i}"), ignore_errors=True) for i in range(1, 8)]
    qza.remove_dir()
    trim.remove_dir()
    web_container.add_emperor()


