#! /usr/bin/perl

#Script for checking FASTQ_Sequences for invalid NucleotideLetters within the sequence. Also compatible with inputting gzipped data.

use IO::Compress::Gzip qw(gzip $GzipError);
use warnings;
use strict;
use Getopt::Std;

###################################################################################################
#Parse Options and check
###################################################################################################

my %options=();
getopts("i:z:m:o:h", \%options) or print_help();
print_help() if $options{h};

#check if input file is readable
unless ( -r $options{i} ) { warn "ERROR: Inputfile not readable\n"; print_help(); }

#check if outputpath is given
unless ( $options{o}) { warn "ERROR: No Outputpath given\n"; print_help(); }

#check if mode is given
unless ( $options{m} eq "replace" || $options{m} eq "delete") { warn "ERROR: No or unknown mode given\n"; print_help(); }

##############################################################################################
#check for type of compression and set corresponding variable for later uncompression

#var to store uncompression method
my $uncompress;

#checking type of compression from inputfile
if ($options{i} =~ /\.zip$/i) {
 	$uncompress = '/usr/bin/unzip -p'; # -p = pipe output to STDOUT
} elsif ($options{i} =~ /\.gz$/i) {
 	$uncompress = '/bin/gzip -cd'; # -d = decompress; -c = write to STDOUT
} elsif ($options{i} =~ /\.bz2$/i) {
 	$uncompress = '/usr/bin/bzip2 -cd';
}

#open filehandle for input dependend on compression type
my $FASTQIN_path = $options{i};
my $FASTQIN;
if ($uncompress) 
{
 	open $FASTQIN, "$uncompress $FASTQIN_path|" or die "ERROR: couldn't open file '$FASTQIN_path' : $!";
} 
else 
{
 	open $FASTQIN, "$FASTQIN_path" or die "ERROR: couldn't open file '$FASTQIN_path' : $!";
}

##############################################################################################
#check for ambigious nucleotides and replace with 'N' or delete whole readblock dependend on $options{m}

#open filehandle for output depended if compression is set or not
my $OUT;
if ($options{z})
{
	open $OUT, "| gzip -c > $options{o}" or die "ERROR: couldn't write to File '$options{o}";
}
else
{
	open $OUT, ">$options{o}" or die "ERROR: couldn't write to File '$options{o}";
}

#set counter for counting Nr of reads with replacements/deletions done
my $count = 0;
my @fastqblock;
#initiate array to safe each block and initiate marker for printout
my $printout = 0;

while (my $line = <$FASTQIN>)
{
	#check every block of 4 lines and print out if nothing was found, else don't print (delete) or replace
	#first line is ID of read (e.g. @CP000360-_Acidobacteria_994449/1)
	if (($. == 1) || ($.-1)%4 == 0)
	{
		$fastqblock[0] = $line;
	}
	#second line contains nucleotide sequence, if wrong match found, handel line dependend on mode set;
	if (($. == 2) || ($.-2)%4 == 0 )
	{
		if ( $line =~ /((?![ACGT]).)/)
		{
			if ($options{m} eq "delete")
			{
				$count++;
				$printout = 0;
			}
			elsif ($options{m} eq "replace")
			{
				$line =~ s/((?![ACGT]).)/N/g;
				$fastqblock[1] = $line;
				$count++;
				$printout = 1;
			}
		}
		else
		{
			$printout = 1;
			$fastqblock[1] = $line;
		}
	}
	#third line contains + with possible ID again
	if (($. == 3) || ($.-3)%4 == 0)
	{
		$fastqblock[2] = $line;
	}
	#fourth line contains qualities to corresponding nucleotide sequence
	if (($. == 4) || ($.-4)%4 == 0)
	{
		$fastqblock[3] = $line;
		
		#if printout in second block got set to 1, print out block, else discard and move to next block
		if ($printout)
		{
			print $OUT @fastqblock;
		}
	}
}

print "Number of lines with ambigious/invalid nucleotides found: $count\n";

close $FASTQIN;
close $OUT;



###################################################################################################
#subroutines
###################################################################################################

sub print_help
{
	print STDERR <<EOD;

Usage: $0 [Parameters]
	
	Perl Program to check and correct ambigious nucleotide codes in FASTQ-Files, also in zipped format
	Possible input: unzipped, .gz, .bz2, .zip
	
	Parameters:
	
	-i	Inputfile
	-o	Outputfile
	-z	[true]				set to true if output should be gzipped (optional)
	-m	[replace|delete]	set mode what to do when ambigious nucleotide found (replace with N, delete whole block)
	
	-h	Prints this helpmessage
	
EOD
	exit;
}
