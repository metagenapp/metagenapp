#! /usr/bin/perl
#Samuel Gerner, October 2015
#Script to select hits from result file of hmmer 3.1b out of corresponding fasta file and write them to new FASTA file
#additionally write out all sequences with no hmmerhits (i.e. rRNA depleted FASTA file)


use warnings;
use strict;
use Getopt::Std;


###################################################################################################
#Parse Options and check
###################################################################################################

my %options=();
getopts("i:o:p:h", \%options) or print_help();
print_help() if $options{h};

#check if input FASTA file is readable
unless ( -r $options{i} ) { warn "ERROR: FASTA inputfile not readable\n"; print_help(); }

#check if outputpath is given
unless ( $options{o}) { warn "ERROR: No Outputpath given\n"; print_help(); }


#################################################
#global settings and vars
my %hmmerhits;
my $hmmer_mode;
my $eval_threshold;
if ($options{p}) {
	$eval_threshold = $options{p}
}
else {
	$eval_threshold = 0.1;		#default threshold
}
my $evalpos;
my $typepos;
my $duplicates = 0;
my $duplicates2 = 0;


###################################################################################################
#search all result files for hits and store them in %hmmerhits

#all hmmerresult files are supposed to be in @ARGV 
foreach (@ARGV) {
	
	my @filenamearr = split '\/', $_;
	my $filename = $filenamearr[-1];
	
	#check if file is readable
	unless ( -r $_ ) {
		warn "WARNING: File '$_' nor readable, skipping file\n";
		next;
	}
	
	#save current number of duplicates for comparison
	$duplicates2 = $duplicates;
	
	print "Reading $filename...\t";
	read_hmmerresult($_);
	#if hits found in duplicates print out number of discarded hits
	if(($duplicates - $duplicates2) > 0 ) {
		my $newdups = $duplicates - $duplicates2;
		print "$newdups hits in '$filename' were ignored because being duplicates\n";
	}
}

#print number of hits found in hmmerfile
my $count = scalar keys %hmmerhits;
print "Nr of profile hits from hmmer files: $count\n";


###################################################################################################
#check for type of compression of FASTA file and set corresponding variable for uncompression

#var to store uncompression method
my $uncompress;

#checking type of compression from inputfile
if ($options{i} =~ /\.zip$/i) {
 	$uncompress = '/usr/bin/unzip -p'; # -p = pipe output to STDOUT
} elsif ($options{i} =~ /\.gz$/i) {
 	$uncompress = '/bin/gzip -cd'; # -d = decompress; -c = write to STDOUT
} elsif ($options{i} =~ /\.bz2$/i) {
 	$uncompress = '/usr/bin/bzip2 -cd';
}

#open filehandle for FASTA input dependend on compression type
my $FASTAIN_path = $options{i};
my $FASTAIN;
if ($uncompress) 
{
 	open $FASTAIN, "$uncompress $FASTAIN_path|" or die "ERROR: couldn't open file '$FASTAIN_path' : $!";
} 
else 
{
 	open $FASTAIN, "$FASTAIN_path" or die "ERROR: couldn't open file '$FASTAIN_path' : $!";
}

#open output filehandle
my $OUT_path = $options{o};
open my $OUT, ">$OUT_path" or die "ERROR: couldn't write to file '$OUT_path' : $!";

#open output filehandle for negative hits
my $OUTneg_path = $options{o};
#check if filename given contains dot, else just append postfix
if ($OUTneg_path =~ /\./) {
	$OUTneg_path =~ s/\./\.neg\./;
}
else {
	$OUTneg_path = $OUTneg_path . ".neg";
}
open my $OUTneg, ">$OUTneg_path" or die "ERROR: couldn't write to file '$OUTneg_path' : $!";

#set variables for FASTA file loop
my $counthits = 0;
my $hit = 0;
my $countneghits= 0;

while (my $line = <$FASTAIN>)
{
	my @linearr;	
	#check for header line
	#expected input format is FASTA, with ID captured before directly up to next whitespace
	#e.g.: ">AE016822-_Actinobacteria_7078_2"
	if ($line =~ />/) {
		#remove fasta header from line
		$line =~ s/>//;
		@linearr = split '\s+', $line;
		
		#check if current ID exists in hits from hmmerfile, if found print to result file and set marker $hit for next line containing sequence
		if (exists $hmmerhits{$linearr[0]})
		{
			$hit = 1;
			$counthits++;
			print $OUT ">$line";
		}
		else {
			$hit = 2;
			$countneghits++;
			print $OUTneg ">$line";
		}
	}
	else {
		#if header before was found as a hit, print out sequence after header was printed before
		if ($hit == 1) {
			print $OUT $line;
		}
		elsif ($hit == 2) {
			print $OUTneg $line;
		}
	}
}
close $FASTAIN;
close $OUT;

print "Nr of hits found again in FASTA file: $counthits\n";
print "Nr of remaining sequences in FASTA file: $countneghits\n";
if ($count != $counthits) {
	warn "WARNING: Number of expected hits in FASTA file differs from hmmer result, please check input files\n";
}



###################################################################################################
#subroutines
###################################################################################################

sub print_help
{
	print STDERR <<EOD;

Usage:	$0 [Parameters] [Input hmmer result file(s)]
		e.g. $0 -i infile.fa -o outfile.fa hmmerresult1 hmmerresult2 ...
	
	Perl Program to select hits from hmmer 3.1b (hmmsearch, nhmmer) out of corresponding fasta file and 
	write detected sequences into new fasta file
	
	hmmerresult files have to come from --tblout option of hmmer
	
	Parameters:
	
	-i	Input FASTA file of hmmer search (format .fa, .fa.gz, .fa.bz2, .fa.zip)
	-o	Output FASTA file name with hits(two Outputs, one with hits and one without)
			second File with <basename>.neg containing remaining sequences will be created
	
	optional
	-p	e-value threshold (e.g. '0.1'), default 0.1
	
	-h	Prints this helpmessage
	
EOD
	exit;
}

#################################################
#open hmmer result, check for type of hmmersearch (nhmmer, hmmsearch) using header, then save all hits 
#with e-value threshold into hash for later filtering of fasta file.


sub read_hmmerresult
{
	#get filepath
	my $FH_path = shift @_;
	open my $FH, "$FH_path" or die "ERROR: couldn't open file '$FH_path' : $!";
	
	while (my $line = <$FH>)
	{
		#result of hmmsearch --tblout has additional info in first line and string target name occurs in 2nd line, at nhmmer already occurs in 1st line
		if ($. == 1) 	{
			if ($line =~ /target/) 		{
				$hmmer_mode = "nhmmer";
				print "Detected nhmmer input...\n";
				#position of evalue column in @linearr
				#(nhmmer) expected format described in line nr.1 is
				## target name | accession | query name | accession | hmmfrom | hmm to | alifrom | ali to | envfrom | env to | sq len | strand | E-value | score | bias | description of target
				$evalpos = 12;
				$typepos = 2;
			} 
			else {
				$hmmer_mode = "hmmsearch";
				print "Detected hmmsearch input...\n";
				#position of evalue column in @linearr
				#(hmmer) expected format described in line nr.2 is
				### target name | accession | query name | accession | E-value | score | bias | E-value | score | bias | exp | reg | clu | ov | env | dom | rep | inc | description of target
				$evalpos = 4;
				$typepos = 2;
			}	
		}
		
		#skip comment lines
		next if ($line =~ /#/);
		
		#split line into separate columns, which are whitespace separated (not tabdel!!)
		my @linearr = split '\s+', $line;
		
		#check if readname already exists in hash and throw Warning
		if (exists $hmmerhits{$linearr[0]}) {
			#warn "WARNING: Duplicate Read ID '$linearr[0]' found at linenr $. in hmmerresult file, read will be discarded\n";
			$duplicates++;
		}
		
		#save names of reads which fall under eval threshold
		if ($linearr[$evalpos] <= $eval_threshold) {
			$hmmerhits{$linearr[0]} = {
				eval_threshold => $linearr[$evalpos],
				type => $linearr[$typepos]
			};
		}
		#else {
		#	print "Filter failed: $linearr[0] with eval $linearr[$evalpos]\n";
		#}
	}
	close $FH;
}