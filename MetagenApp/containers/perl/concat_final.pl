#! /usr/bin/perl

###################################################################
############ concatenate fasta based on headers ###################
################### Sandra Hauzmayer ##############################
###################################################################

use warnings;
use strict;



my $file1 = "$ARGV[0]" ;
my $file2 = "$ARGV[1]" ;
my $concat1 = "$ARGV[2]"."_nomatch_1.fa" ;
my $concat2 = "$ARGV[2]"."_nomatch_2.fa" ;
my $concat3 = "$ARGV[2]"."_match.fa" ;
my %line_array1 ;
my %line_array2;

my $curtime = localtime;
print "\n$curtime\tStarting to concat \n\t\t\t\tRuntime: " . runtime(time - $^T) . "\n";

open my $FASTAIN1, "$file1" or die "ERROR: couldn't open file '$file1' : $!";
open my $FASTAIN2, "$file2" or die "ERROR: couldn't open file '$file2' : $!";
open my $OUT_FH1, '>', $concat1 or die "Can not open $concat1 $!\n";
open my $OUT_FH2, '>', $concat2 or die "Can not open $concat2 $!\n";
open my $OUT_FH3, '>', $concat3 or die "Can not open $concat3 $!\n";


while (<$FASTAIN1>) {
	chomp $_ ;
	if ( /^>.*[12]$/ ) { chop }
	my $test = <$FASTAIN1> ;
	chomp $test ;
	$line_array1{$_} = $test; 
}
close $FASTAIN1 ;

#print %line_array1 ;
while (<$FASTAIN2>) {
	chomp $_ ;
	if ( /^>.*[12]$/ ) { chop }
	my $test = <$FASTAIN2> ;
	chomp $test ;
	$line_array2{$_} = $test;
}
close $FASTAIN2 ;

foreach my $key1 (keys %line_array1) {
		if ( ! exists $line_array2{$key1} ) {
			print  $OUT_FH1 "$key1\n$line_array1{$key1}\n" ;
			#splice(@line_array1, $i, 2);	
		}
		else {
			print  $OUT_FH3 "$key1\n$line_array1{$key1}N$line_array2{$key1}\n" ;
			delete $line_array1{$key1};
			delete $line_array2{$key1};
		}

}

foreach my $key2 (keys %line_array2) {
	print  $OUT_FH2 "$key2\n$line_array2{$key2}\n" ;
}

close $OUT_FH1 ;
close $OUT_FH2 ;
close $OUT_FH3;
$curtime = localtime;
print "\n$curtime\t DONE! \n\t\t\t\tRuntime: " . runtime(time - $^T) . "\n";



######################################################################################################
sub runtime
{
	#get runtime in seconds from "(time - $^T)"
	my $time = shift @_;
	my $rtime;
	#check if script ran for more than one minute
	if ($time > 59 ) {
		#or more than one hour
		if ($time > 3599 ) {
			#or more than one day
			if ($time > 86399) {
				$rtime = int($time / 86400) . "d " . int(($time % 86400) / 3600) . "h " . int((($time % 86400) % 3600) / 60) . "m " . (((time % 86400) % 3600) % 60) . "s";
				return $rtime;
			}
			$rtime = int($time / 3600) . "h " . int(($time % 3600) / 60) . "m " . (($time % 3600) % 60) . "s";
			return $rtime;	
		}
		$rtime = int($time / 60) . "m " . ($time % 60) . "s";
		return $rtime;
	}
	$rtime = $time . "s";
	return $rtime;
}







