#!/usr/bin/perl

use strict ;
use warnings;
use Getopt::Std;

# USAGE definition
############################################################

my %options=();
getopts("ho:g:b:m:c:", \%options);
my $outfile ;
my $obo_file ;
my @abundance_files = () ;

my $USAGE = <<"USAGE";
Usage: ./go_slim_term_extract.pl -b <filename> -m <filename> -c <filename> -g <filename> -o <filename>

Regular options:

-h\t-- prints this help message
-o\t-- output file name
-g\t-- obo file with GO slim terms defined
-b\t-- biological process input file
-m\t-- molecular function input file
-c\t-- cellular component input file

USAGE

# help menue
die "$USAGE" if defined $options{h};
# output file
die "output directory not defined, please specify with -o\n" unless  defined $options{o}  ;
$outfile=$options{o} ;
# check if obo file (go slim terms) were specified
die "go slim term file not defined, please specify with -g\n" unless  defined $options{g}  ;
$obo_file=$options{g} ;
die "biological process input file not defined, please specify with -b\n" unless  defined $options{b}  ;
push @abundance_files , $options{b};
die "molecular function input file not defined, please specify with -m\n" unless  defined $options{m}  ;
push @abundance_files , $options{m};
die "cellular component input file not defined, please specify with -c\n" unless  defined $options{c}  ;
push @abundance_files , $options{c};

###########################################################

my %abundance_info ;

open my $FASTAIN1, "$obo_file" or die "ERROR: couldn't open file '$obo_file' : $!";
open my $FASTAOUT, '>', "$outfile" or die "ERROR: couldn't open file '$outfile' : $!";

foreach my $file (@abundance_files) {
	open my $FASTAIN2, "$file" or die "ERROR: couldn't open file '$file' : $!";
	while (<$FASTAIN2>) {
		chomp $_ ;
		if ( /^(GO:\d+)\s+\w+.*\s+(\d+\.\d+.*)/ ) { 
			#print "$1 : $2\n" ;
			$abundance_info{$1} = $2;
		}
	}
	close $FASTAIN2 ;
}


print $FASTAOUT print_header() ;

my @tmp_arr = <$FASTAIN1> ;

for ( my $i = 0 ; $i < @tmp_arr ; $i++)  {
	if ( $tmp_arr[$i] =~ /^\[Term\]/ ) {
		my $id =join ("" , $tmp_arr[$i+1]=~m/(GO:\d+)/ );
		my @name = $tmp_arr[$i+2]=~m/^name:\s(\w+.*\w$)/ ;
		my @namespace = $tmp_arr[$i+3]=~m/^namespace:\s(\w+_\w+$)/ ;
		if ($namespace[0] eq "molecular_function" ) {
			print $FASTAOUT "$id\tmolecular_function\t@name" ; 
			print $FASTAOUT add_abundance($abundance_info{$id}) ;
		}
		elsif ($namespace[0] eq "biological_process") {
			print $FASTAOUT  "$id\tbiological_process\t@name" ;
			print $FASTAOUT add_abundance($abundance_info{$id}) ;
		}
		elsif ($namespace[0] eq "cellular_component") {
			print $FASTAOUT  "$id\tcellular_component\t@name" ;
			print $FASTAOUT add_abundance($abundance_info{$id}) ;
		}
		else {
			print "unknown namespace $namespace[0]!\n"
		}
		
			
	}

 }

close $FASTAIN1 ;
close $FASTAOUT ;


sub add_abundance {
	my $id_string = $_[0] ;
	#print "$_[0]\n" ;
	if ($id_string ) {
		return "\t$id_string\n" ;
	}
	else {
		return "\t0\n" ;
	}
}

sub print_header {

	return "#ID\tcomponent\tName\tAbundance\n"

}




