import MetagenApp.config as config


class WebContainer:
    """
    override methods for docker container objects
    """

    def __init__(self, container):
        self._container = container
        self._id = container.id

    @property
    def container(self):
        return self._container

    @property
    def id(self):
        return self._id

    def stop(self):
        return self._container.stop()

    def remove(self):
        return self._container.remove()

    def exec(self, scriptname, *args, logging=True):
        """
                docker exec script in /web/django/scripts using django-extensions
        :return:
        docker exec script in /web/django/scripts using django-extensions
        :param scriptname: name of script (without extension)
        :param args: script arguments
        :param logging: bool
        :return:
        """
        arg_str = "--script-args " + " ".join([str(arg) for arg in args]) if args else ""
        cmd = f"python manage.py runscript {scriptname} {arg_str} --traceback"
        if logging:
            try:
                config.main_logger.info(cmd)
                exit_code, demux = self.container.exec_run(cmd, stdout=True, stderr=True, demux=True )
                out, err = demux
                if out:
                    config.main_logger.info(out.decode())
                if err:
                    config.main_logger.error(err.decode())

            except Exception as e:
                config.main_logger.error(e)
                config.main_logger.exception(e)
        else:
            self.container.exec_run(cmd)

    def add_run_and_sample(self):
        """
        Add pipeline_pipeline_run and pipeline_sample to db by calling django runscript
        :param config: instance of Config
        :return: updated Config instance
        """
        if not config.db_connector.check_existing_run():
            self.exec("add_run_and_sample", config.run_image)
            run = config.db_connector.fetch_run("new")
            config.set("run_id", run[0])
            config.set("date", run[5])

            return run

        else:
            print("Pipeline run not started.\nAnalysis name already in use!")
            exit()

        # self.exec("add_run_and_sample")
        # run = config.db_connector.fetch_run("failed")
        # config.set("run_id", run[0])
        # config.set("date", run[4])
        #
        # return run

    def add_fastqc(self, samples):
        for sample in samples:
            self.exec("add_fastqc", sample.name, sample.fwd, sample.rev, config.run_id, config.init, config.container_django_dir)

    def add_krona(self, samples):
        for sample in samples:
            self.exec("add_krona", sample.name, config.run_id, config.init, config.container_django_dir)

    def add_emperor(self):
        self.exec("add_emperor", config.run_id, config.init, config.container_django_dir)

    def add_taxonomy_wgs(self, samples):
        for sample in samples:
            self.exec("add_taxonomy_wgs", sample.name, config.run_id, config.init, config.container_django_dir)

    def add_taxonomy_16S(self, samples):
        for sample in samples:
            self.exec("add_taxonomy_16S", sample.name, config.run_id, config.init, config.container_django_dir)

    def get_taxonomy_table(self, table):
        self.exec("get_taxonomy_table", table, config.run_id, config.init, config.container_django_dir)

    def get_metadata(self):
        self.exec("get_metadata", config.run_id, config.init, config.container_django_dir)

    def krakenreport2kronatxt(self, samples):
        for sample in samples:
            self.exec("krakenreport2kronatxt", config.run_id, sample.name, config.init, config.container_django_dir)

    def add_log(self, level, tool, sample, message):
        self.exec("add_log", config.run_id, level, tool, sample, f"'{message}'", logging=False)

    def dump_run(self, run_id):
        self.exec("dump_run", run_id, config.init, config.container_django_dir)
