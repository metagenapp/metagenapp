import re
from collections import namedtuple
from MetagenApp import config
import shutil
from pathlib import Path


class Samples(list):
    def __init__(self, *args):
        super(Samples, self).__init__(args[0])
        self.name = "all"

    @classmethod
    def collect(cls, tool, regex, pe=False):
        samples = []
        tool_path = config.input_directory / tool
        samplelist = [x for x in tool_path.iterdir()]
        for s in samplelist:
            samplefiles = []
            container_dir = tool + "/" + str(s.name)
            sample_path = tool_path / s
            for path in sample_path.glob('**/*'):
                if re.match(regex, path.name):
                    fn = str(path)[len(str(sample_path)) + 1:]
                    samplefiles.append(fn)
            if len(samplefiles) == 0:
                raise IndexError("No Samplefiles in Sample object")
            elif pe:
                samplefiles.sort()
                fwd_path = f"/data/{container_dir}/{samplefiles[0]}"
                rev_path = f"/data/{container_dir}/{samplefiles[1]}"
                Sample = namedtuple("sample",
                                    ["name", "host_dir", "container_dir", "fwd", "rev", "fwd_path", "rev_path"])
                samples.append(
                    Sample(s.name, config.input_directory, container_dir, samplefiles[0], samplefiles[1], fwd_path,
                           rev_path))

            elif len(samplefiles) == 1:
                path = f"/data/{container_dir}/{samplefiles[0]}"
                Sample = namedtuple("sample", ["name", "host_dir", "container_dir", "file", "path"])
                samples.append(Sample(s.name, config.input_directory, container_dir, samplefiles[0], path))

            else:
                paths = [f"/data/{container_dir}/{s}" for s in samplefiles]
                Sample = namedtuple("sample", ["name", "host_dir", "container_dir", "files", "paths"])
                samples.append(Sample(s.name, config.input_directory, container_dir, samplefiles, paths))

            samples = sorted(samples, key=lambda x: x.name)

        return cls(samples)

    @classmethod
    def move_files(cls, tool, regex, new_postfix, pe=False):
        samples = cls.collect(tool, regex, pe=pe)
        for sample in samples:
            if pe:
                shutil.move(Path(sample.host_dir / sample.container_dir / sample.fwd),
                            Path(sample.host_dir / sample.container_dir).joinpath(sample.name + "_fwd_" + new_postfix))
                shutil.move(Path(sample.host_dir / sample.container_dir / sample.rev),
                            Path(sample.host_dir / sample.container_dir).joinpath(sample.name + "_rev_" + new_postfix))
            elif sample.file:
                shutil.move(Path(sample.host_dir / sample.container_dir / sample.file),
                             Path(sample.host_dir / sample.container_dir).joinpath(sample.name + new_postfix))
            elif sample.files:
                raise NotImplementedError("Cannot rename multipe files at once!")

    def remove_files(self):
        for sample in self:
            if hasattr(sample, "fwd"):
                Path(sample.host_dir / sample.container_dir / sample.fwd).unlink()
                Path(sample.host_dir / sample.container_dir / sample.rev).unlink()
            elif hasattr(sample, "file"):
                Path(sample.host_dir / sample.container_dir / sample.file).unlink()
            elif hasattr(sample, "files"):
                for file in sample.files:
                    Path(sample.host_dir / sample.container_dir / file).unlink()

    def remove_dir(self):
        sample = self[0]
        shutil.rmtree(Path(sample.host_dir / sample.container_dir.split("/")[0]))