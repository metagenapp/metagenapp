import time
from pathlib import Path
from MetagenApp import config
from MetagenApp.pipeline import start_run
from docker import APIClient
from MetagenApp.db_connector import DbConnector


class Worker:
    def __init__(self, dockerclient, db_container,  web_container, ngnix_container, network):
        self.client = dockerclient
        self.metagen_net = network  # user defined docker network
        self.db_container = db_container
        self.web_container = web_container
        self.ngnix_container = ngnix_container

    def work(self):
        """
        polling db for pipeline_run relations with status 'new'
        :return:
        """

        d = APIClient()
        logs = d.logs(self.web_container.id, stream=True)
        if config.analysis:
            config.db_connector.update_run("started")
            start_run(self.web_container, self.client)
            self.web_container.dump_run(config.run_id)

        while True:
            for log in logs:
                try:
                    print(log.decode(), end="")
                except AttributeError:
                    print(log, end="")

            config.set("db_connector", DbConnector(set_ip=True))
            run = config.db_connector.fetch_run("started")
            if run:
                config.from_worker(run, Path.cwd() / "input" / run[1])
                start_run(self.web_container, self.client)
                config.db_connector.update_run("done")
                self.web_container.dump_run(config.run_id)

            time.sleep(1)

    def stop_work(self):
        """
        cleanup when worker is stopping
        :return:
        """
        print("Gracefully Stopping and Removing metagen-containers")
        containerlist = self.client.containers.list(all=True)
        containerlist = [c for c in containerlist if c.name.startswith("metagen_")]
        [print(f"Stopping:{c.name}") for c in containerlist]
        [c.stop(timeout=5) for c in containerlist]
        [print(f"Removing:{c.name}") for c in containerlist]
        [c.remove(force=True) for c in containerlist]
        print("Removing metagen-network")
        self.metagen_net.remove()
        print("Exit.")
