import uuid
from collections import namedtuple
from pathlib import Path
from shutil import copyfile

import yaml

from MetagenApp.db_connector import DbConnector
from MetagenApp.logger import setup_logger
from MetagenApp.samplelist import Samples

init = None
run_id = None
run_image = None
analysis = None
analysis_de = None
mode = None
date = None
status = None
input_directory = Path.cwd() / "input"
data_directory = Path.cwd() / "MetagenApp/data/"
postgres_volume_directory = Path.cwd() / "postgresvolume"
db_connector = None
pg_port = 5432
ngnix_port = 80
samples = []
sample_cnt = 0
max_cpu = 1
max_container = 1
max_threads = 1
kraken2_db = data_directory / "minikraken2_v2_8GB_201904_UPDATE"
metaphlan2_db = data_directory / "mpa_v20_m200"
humann2_chocophlan = data_directory / "chocophlan"
humann2_uniref = data_directory / "uniref"
silva_qiime = data_directory / "silva_qiime"
dockerclient = None

primer_16S_fwd = None
primer_16S_rev = None

GRCh38 = Path.cwd() / "MetagenApp/data/GRCh38"
main_logger = None
readlen = None
cleanup = "all"
kmer_len = 35
container_django_dir = "/var/www/"

trim_leading = 20
trim_trailing = 20
trim_minlength = 70
trim_sliding_window = 4
trim_sliding_qual = 15
trim_crop = 0
trim_head_crop = 0
trim_illumina_clip = "No Clipping"
trim_palindrome_clip_theshold = 20
trim_seed_mismatches = 20
trim_simple_clip_theshold = 30
trim_minAdapterLength = 0


def set(name, value):
    try:
        globals()[name] = value
    except KeyError:
        pass


def set_thread_pool_values(max_cpu_cores, sample_nr):
    """
    optimise max_container and max_threads (per container) so all given
    cpu_cores are used by the thread pool
    :param max_cpu_cores:
    :param sample_nr:
    :return:
    """
    if max_cpu_cores % sample_nr == 0:
        set("max_container", sample_nr)
        set("max_threads", int(max_cpu_cores / sample_nr))

    else:
        return set_thread_pool_values(max_cpu_cores, sample_nr - 1)


def set_main_logger():
    log_path = Path.cwd() / f"MetagenApp/logs/{analysis}"
    log_path.mkdir(exist_ok=True)
    logger = setup_logger("logger", log_path / "run.log")
    globals()["main_logger"] = logger

    return logger


def from_configfile(config_fo):
    """
    Init Config class object from configfile
    :param config_fo: file object from config.yaml
    :return: Config class instance
    """
    set("db_connector", DbConnector())
    set("init", "configfile")
    config_io = config_fo.read()
    config_fo.close()
    Path.mkdir(Path.cwd() / "web/django/tmp", exist_ok=True)
    with open(str(Path.cwd() / "web/django/tmp/config.yaml"), "w+") as fh:
        fh.write(config_io)

    c = yaml.load(config_io, Loader=yaml.SafeLoader)
    if "postgres_volume_directory" in c.keys():
        set("postgres_volume_directory", Path(c.pop("postgres_volume_directory")))

    descr = c.pop("description") if "description" in c.keys() else ""
    descr_de = c.pop("description_de") if "description_de" in c.keys() else ""

    details = c.pop("details") if "details" in c.keys() else ""
    details_de = c.pop("details_de") if "details_de" in c.keys() else ""

    set("analysis", c["analysis"])
    analysis_de = c.pop("analysis_de") if "analysis_de" in c.keys() else ""
    if not analysis_de:
        analysis_de = c["analysis"]
    set_main_logger()
    input_dir = c.pop("input_directory")
    input_dir = Path(input_dir) if input_dir.startswith("/") else Path.cwd() / input_dir
    img_fp = input_dir.joinpath(c["image"]) if "image" in c.keys() else None
    nf = "run_image_default.png"
    if img_fp:
        if img_fp.is_file():
            copyfile(str(img_fp),
                     str(Path.cwd() / f"web/django/media/{c['image']}"))
            nf = c["image"]
        else:
            uid = uuid.uuid4()
            nf = f"{img_fp.stem }_{str(uid)}.jpg"
            new_fp = Path.cwd() / f"web/django/media/{nf}"
            copyfile(str(img_fp), new_fp)

    set("run_image", nf)
    set("input_directory", input_dir)

    run = ("",
           analysis, analysis_de,
           c["mode"],
           c["max_cores"],
           "",
           "new",
           input_directory,
           descr, descr_de,
           details, details_de,
           nf)

    Sample = namedtuple("sample", ["name", "host_dir", "fwd", "rev"])
    set("samples",
        Samples([Sample(str(s["name"]), input_directory, s["files"][0], s["files"][1]) for s in c.pop("samples")]))
    set("max_cpu", run[4])
    set("status", run[6])
    set_thread_pool_values(max_cpu, len(samples))
    if globals()["max_container"] > 8:
        set("max_container", 8)
        set("max_threads", int(max_cpu / 8))

    [set(k, v) for k, v in c.items()]


def from_worker(run, _input_directory):
    """
    Init Config class object from worker
    :param run: tuple with pipeline_run relation
    :param samples: tuple with pipeline_sample relation
    :param input_directory: directory where uploaded files are saved
    :return: Config class instance
    """
    set("init", "worker")
    set("run_id", run[0])
    set("analysis", run[1])
    set_main_logger()
    set("mode", run[2])
    set("max_cpu", run[3])
    set("date", run[4])
    set("status", run[5])
    set("input_directory", _input_directory)
    set("samples", Samples(db_connector.fetch_samples(_input_directory)))

    trim = db_connector.fetch_trim_params(run_id)

    set("trim_leading", trim[0])
    set("trim_trailing", trim[1])
    set("trim_minlength", trim[2])
    set("trim_sliding_window", trim[3])
    set("trim_sliding_qual", trim[4])
    set("trim_illumina_clip", trim[5])
    if trim[5] != "No Clipping":
        trim_clip = db_connector.fetch_trimclip_params(run_id)
        set("trim_seed_mismatches", trim_clip[0])
        set("trim_palindrome_clip_theshold", trim_clip[1])
        set("trim_simple_clip_theshold", trim_clip[2])
        set("trim_minAdapterLength", trim_clip[3])
