import docker.models.containers
from docker.types import IPAMPool, IPAMConfig


def start_network(client):
    """
    creates a user defined network for docker containers
    :return: docker models.networks.Network
    """
    ipam_pool = IPAMPool(subnet='172.18.0.0/16',
                         gateway='172.18.0.1')

    ipam_config = IPAMConfig(pool_configs=[ipam_pool])

    metagen_net = client.networks.create("metagen_net",
                                         driver="bridge",
                                         ipam=ipam_config)
    return metagen_net
