import bz2
import os
import platform
import wget
import zipfile
from pathlib import Path

import docker
from docker.errors import ImageNotFound

from MetagenApp import config
from os import listdir, rmdir
from os.path import join
from shutil import move


def human_ref_loader():
    image = "quay.io/biocontainers/metaphlan2:2.7.8--py27h24bf2e0_0"
    image_name = image.split("/")[-1].split(":")[0]
    print("Building GRCh38 index...", end="")

    kwargs = {"network": "metagen_net",
              "detach": False,
              "name": "_".join(["metagen", image_name]),
              "volumes": {config.GRCh38: {"bind": "/db/", "mode": "rw"}},
              "remove": True,
              "command": f"bowtie2-build --threads {config.max_cpu} /db/GRCh38.15_GRCh38_genomic.fna /db/GRCh38",
              "stdout": True,
              "stderr": True}

    if not platform.system() == "Windows":
        kwargs["user"] = os.getuid()

    try:
        log = config.dockerclient.containers.run(image, **kwargs)
        config.GRCh38.joinpath("GRCh38.15_GRCh38_genomic.fna").unlink()
        print("done!")
        config.main_logger.info(log.decode())

    except docker.errors.ContainerError as e:
        print("error!")
        config.main_logger.error(e.stderr.decode("utf-8"))


def humann2_loader():
    image = "quay.io/biocontainers/humann2:0.11.1--py27_3"
    image_name = image.split("/")[-1].split(":")[0]
    print("Building humann2 database...", end="")
    kwargs = {"network": "metagen_net",
              "detach": False,
              "name": "_".join(["metagen", image_name]),
              "volumes": {config.humann2_uniref.parent: {"bind": "/db/", "mode": "rw"}},
              "remove": True,
              "command": "humann2_databases --download chocophlan full /db/",
              "stdout": True,
              "stderr": True}

    if not platform.system() == "Windows":
        kwargs["user"] = os.getuid()

    try:
        log = config.dockerclient.containers.run(image, **kwargs)

        print("done!")
        config.main_logger.info(log.decode())

    except docker.errors.ContainerError as e:
        print("error!")
        config.main_logger.error(e.stderr.decode("utf-8"))


def metaphlan2_loader():
    for path, dirs, files in os.walk(config.metaphlan2_db):
        for file in files:
            if file.endswith(".fna.bz2"):
                filepath = os.path.join(path, file)
                newfilepath = filepath[:-4]
                with open(newfilepath, 'wb') as new_file, bz2.BZ2File(filepath, 'rb') as infile:
                    for data in iter(lambda: infile.read(100 * 1024), b''):
                        new_file.write(data)
                cmd = f"bowtie2-build --threads {config.max_cpu} /db/{file[:-4]} /db/{file[:-8]}"
                client = docker.from_env(timeout=99999)
                try:
                    kwargs = {"image": "quay.io/biocontainers/metaphlan2:2.7.8--py27h24bf2e0_0",
                              "command": cmd,
                              "volumes": {config.metaphlan2_db: {"bind": "/db/", "mode": "rw"}}}
                    if not platform.system() == "Windows":
                        kwargs["user"] = os.getuid()
                    log = client.containers.run(**kwargs)

                    config.main_logger.info(log.decode())
                except docker.errors.ContainerError as e:
                    config.main_logger.error(e)

                os.remove(newfilepath)
                os.remove(filepath)


def silva_loader():
    silva_zip = zipfile.ZipFile(config.silva_qiime / "silva_qiime.zip")

    with open(str(Path.cwd() / "MetagenApp" / "data" / "silva_qiime" / "80_core_alignment.fna"), 'wb') as f:
        f.write(silva_zip.read("SILVA_132_QIIME_release/core_alignment/80_core_alignment.fna"))

    with open(str(Path.cwd() / "MetagenApp" / "data" / "silva_qiime" / "silva132_99.fna"), 'wb') as f:
        f.write(silva_zip.read("SILVA_132_QIIME_release/rep_set/rep_set_all/99/silva132_99.fna"))

    with open(str(Path.cwd() / "MetagenApp" / "data" / "silva_qiime" / "taxonomy_7_levels.txt"), 'wb') as f:
        f.write(silva_zip.read("SILVA_132_QIIME_release/taxonomy/taxonomy_all/99/taxonomy_7_levels.txt"))

    image = "qiime2/core:2019.4"
    image_name = image.split("/")[-1].split(":")[0]

    kwargs = {"network": "metagen_net",
              "detach": False,
              "name": "_".join(["metagen", image_name]),
              "volumes": {config.silva_qiime: {"bind": "/db/", "mode": "rw"}},
              "remove": True,
              "stdout": True,
              "stderr": True}

    if not platform.system() == "Windows":
        kwargs["user"] = os.getuid()

    try:
        cmd1 = "qiime tools import --type 'FeatureData[Sequence]' --input-path /db/silva132_99.fna " \
               "--output-path /db/silva132_99.qza"
        cmd2 = "qiime tools import --type 'FeatureData[Taxonomy]' --input-format HeaderlessTSVTaxonomyFormat " \
               "--input-path /db/taxonomy_7_levels.txt --output-path /db/taxonomy_7_levels.qza"

        config.dockerclient.containers.run(image, command=cmd1, **kwargs)
        config.dockerclient.containers.run(image, command=cmd2, ** kwargs)

    except docker.errors.ContainerError as e:
        print("error!")
        config.main_logger.error(e.stderr.decode("utf-8"))

    print("downloading trained classifiers")
    wget.download("https://data.qiime2.org/2019.4/common/silva-132-99-nb-classifier.qza",
                  bar=wget.bar_adaptive, out=str(Path.cwd() / "MetagenApp" / "data" / "silva_qiime"))
    wget.download("https://data.qiime2.org/2019.4/common/silva-132-99-515-806-nb-classifier.qza",
                  bar=wget.bar_adaptive, out=str(Path.cwd() / "MetagenApp" / "data" / "silva_qiime"))

    config.silva_qiime.joinpath("silva_qiime.zip").unlink()


def bracken_loader():
    """
    move files to parent directory
    :return:
    """
    for filename in listdir(join(config.kraken2_db, config.kraken2_db.name)):
        move(join(config.kraken2_db, config.kraken2_db.name, filename), join(config.kraken2_db, filename))
    rmdir(join(config.kraken2_db, config.kraken2_db.name))

    ## not neccessary in new minikraken version
    #     """
    #     download read distribution files for minikraken database. (files for build not avaliable)
    #     :return:
    #     """
    #     wget.download(str("https://ccb.jhu.edu/software/bracken/dl/minikraken2_v1/database100mers.kmer_distrib"),
    #                   bar=wget.bar_adaptive, out=str(Path.cwd() / "MetagenApp/data/minikraken2_v1_8GB"))
    #     wget.download(str("https://ccb.jhu.edu/software/bracken/dl/minikraken2_v1/database150mers.kmer_distrib"),
    #                   bar=wget.bar_adaptive, out=str(Path.cwd() / "MetagenApp/data/minikraken2_v1_8GB"))
    #     wget.download(str("https://ccb.jhu.edu/software/bracken/dl/minikraken2_v1/database200mers.kmer_distrib"),
    #                   bar=wget.bar_adaptive, out=str(Path.cwd() / "MetagenApp/data/minikraken2_v1_8GB"))


def bracken_builder(read_length):
    """
    build read distribution files for braken
    :param read_length: read length of input reads
    :return:
    """
    image = "metagenapp/bracken:latest"
    image_name = image.split("/")[-1].split(":")[0]
    _image_on_client = False

    print(f"Looking for {image} image on client...", end="")
    config.dockerclient.images.get(image)
    print(" done!")

    config.main_logger.info(f"Docker image {image} built from Dockerfile.")
    print("Building read distribution file for bracken")
    kwargs = {"network": "metagen_net",
              "detach": False,
              "name": "_".join(["metagen", image_name]),
              "volumes": {config.kraken2_db: {"bind": "/taxonomy/", "mode": "rw"}},
              "remove": True,
              "command": f"bracken-build -l {read_length} -d /taxonomy/ -k {config.kmer_len} -t {config.max_cpu}",
              "stdout": True,
              "stderr": True}

    if not platform.system() == "Windows":
        kwargs["user"] = os.getuid()

    try:

        log = config.dockerclient.containers.run(image, **kwargs)
        config.main_logger.info(log.decode())

    except docker.errors.ContainerError as e:
        config.main_logger.error(e.stderr.decode("utf-8"), extra={'name_override': "Krona updateTaxonomy"})
