from functools import wraps


def log_name(name):
    """function decorator adding log_name attribue"""
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            return func(*args, **kwargs)
        wrapper.log_name = name
        return wrapper
    return decorator


def dir_name(name):
    """function decorator adding dir_name attribue"""
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            func.dir_name = name
            return func(*args, **kwargs)
        return wrapper
    return decorator


def set_rank(rank):
    """function decorator adding dir_name attribue"""
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            func.rank = rank
            return func(*args, **kwargs)
        return wrapper
    return decorator

#
# def bind_cmd(image_instance, func):
#     """
#     Bind the function *func* to *instance*, with either provided name *as_name*
#     or the existing name of *func*. The provided *func* should accept the
#     instance as the first argument, i.e. "self".
#     """
#     bound_method = func.__get__(image_instance, image_instance.__class__)
#     setattr(image_instance, func.__name__, bound_method)

