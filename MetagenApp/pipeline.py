from MetagenApp import config
from MetagenApp.pipline_wgs import run_wgs
from MetagenApp.pipeline_16S import run_16S
from MetagenApp.pipeline_qc import run_qc
from MetagenApp.config import set_main_logger
from MetagenApp.logger import close_handles
import traceback


def start_run(web_container, dockerclient):
    main_logger = set_main_logger()
    try:
        run_qc(web_container, dockerclient)
        if config.mode == "WGS":
            run_wgs(web_container, dockerclient)
        elif config.mode == "16S":
            run_16S(web_container, dockerclient)
        config.db_connector.update_run("done")
        close_handles(main_logger)

    except Exception as e:
        config.db_connector.update_run("failed")
        main_logger.error(e)
        tb = ''.join(traceback.format_tb(e.__traceback__))
        main_logger.error(tb)

