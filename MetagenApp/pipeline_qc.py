import shutil
from pathlib import Path
from zipfile import ZipFile

from MetagenApp import config
from MetagenApp.data_loaders import bracken_builder
from MetagenApp.decorators import log_name
from MetagenApp.images import PipelineImage
from MetagenApp.samplelist import Samples


def set_read_len():
    sample = Samples.collect("fastqc", ".*fastqc_untrimmed.zip", pe=True)[0]
    file = sample.host_dir / "fastqc" / sample.name / sample.fwd
    fn = [s.fwd for s in config.samples if s.name == sample.name][0]

    rep = ZipFile(file).read(f"{fn.split('.')[0]}_fastqc/fastqc_data.txt").decode()
    rl = [l[len("Sequence length\t"):] for l in rep.split("\n") if l.startswith("Sequence length\t")][0]
    rl = int(rl.split("-")[1] if "-" in rl else rl)

    contrib_file = Path(config.kraken2_db).joinpath(f"database{rl}mers.kmer_distrib")
    if not contrib_file.is_file():
        print(f"File not found: {contrib_file}")
        if config.kraken2_db == Path.cwd() / "MetagenApp/data/minikraken2_v2_8GB_201904_UPDATE":
            minikraken_list = [100, 150, 200]
            if rl not in minikraken_list:
                new_rl = min(minikraken_list, key=lambda x: abs(x - rl))
                config.set("readlen", new_rl)
                config.main_logger.warning(
                    f"Read distribution file for read length {rl} not avaliable for Minikaken Database!\n"
                    f"Setting value for bracken readlength to {new_rl}")
        else:
            try:
                print("running bracken builder")
                bracken_builder(rl)
                config.set("readlen", rl)
            except Exception as e:
                print(e)
                raise Exception("Error building kmer distribution file")

    else:
        config.set("readlen", rl)

    if config.trim_crop != 0:
        config.set("trim_crop", rl - config.trim_crop)


def fastqc_cmd(sample):
    out_path = f"fastqc/{sample.name}"
    return f"fastqc -t {config.max_threads} -f fastq -o {out_path} {sample.fwd} {sample.rev}"


def fastqc_trimmed_cmd(sample):
    out_path = f"fastqc/{sample.name}"
    return f"fastqc -t {config.max_threads} -f fastq -o {out_path} {sample.fwd_path} {sample.rev_path}"


@log_name("trimmomatic")
def trimmomatic_cmd(sample):
    trim_params = f"SLIDINGWINDOW:{config.trim_sliding_window}:{config.trim_sliding_qual} MINLEN:{config.trim_minlength} "
    if config.trim_illumina_clip != "No Clipping":
        al = f":{config.trim_minAdapterLength}" if config.trim_minAdapterLength != 0 else ""
        trim_params += f"ILLUMINACLIP:/usr/local/share/trimmomatic/adapters/{config.trim_illumina_clip}.fa:" \
                       f"{config.trim_seed_mismatches}:{config.trim_palindrome_clip_theshold}:" \
                       f"{config.trim_simple_clip_theshold}{al} "

    if config.trim_leading != 0:
        trim_params += f"LEADING:{config.trim_leading} "

    if config.trim_trailing != 0:
        trim_params += f"TRAILING:{config.trim_trailing} "

    if config.trim_crop != 0:
        trim_params += f"CROP:{config.trim_crop} "

    if config.trim_head_crop != 0:
        trim_params += f"HEADCROP:{config.trim_head_crop} "

    out_base = f"/data/trimmomatic/{sample.name}/{sample.name}"

    return f"trimmomatic PE  -threads {config.max_threads} {sample.fwd} {sample.rev}  -baseout {out_base} {trim_params}"


def merge_fwd_cmd(sample):
    outfile = f"/data/bash/{sample.name}/{sample.name}_fwd_trim_merged.fq"
    return f"cat {sample.paths[0]} {sample.paths[1]} > {outfile}"


def merge_rev_cmd(sample):
    outfile = f"/data/bash/{sample.name}/{sample.name}_rev_trim_merged.fq"
    return f"cat {sample.paths[0]} {sample.paths[1]} > {outfile}"


def run_qc(web_container, dockerclient):
    fastqc = PipelineImage(dockerclient, image="quay.io/biocontainers/fastqc:0.11.8--1")
    trimmomatic = PipelineImage(dockerclient, image="quay.io/biocontainers/trimmomatic:0.38--1")
    bash = PipelineImage(dockerclient, dockerfile="containers/bash")

    config.main_logger.info("Starting pipeline")
    fastqc.run(fastqc_cmd, config.samples, config.max_container)
    try:
        Samples.move_files("fastqc", ".*_fastqc.zip", "fastqc_untrimmed.zip", pe=True)
        shutil.rmtree(Path(config.input_directory).joinpath("?"))
    except FileNotFoundError:
        pass

    set_read_len()
    trimmomatic.run(trimmomatic_cmd, config.samples, config.max_container)

    trim_fwd = Samples.collect("trimmomatic", ".*_1.")
    trim_rev = Samples.collect("trimmomatic", ".*_2.")

    bash.run(merge_fwd_cmd, trim_fwd, 1)
    bash.run(merge_rev_cmd, trim_rev, 1)

    trim_merged = Samples.collect("bash", ".*\_trim_merged.fq", pe=True)
    fastqc.run(fastqc_trimmed_cmd, trim_merged, config.max_container)
    try:
        Samples.move_files("fastqc", ".*_fastqc.zip", "fastqc_trimmed.zip", pe=True)
        shutil.rmtree(Path(config.input_directory).joinpath("?"))
    except FileNotFoundError:
        pass
    try:
        shutil.rmtree(Path(config.input_directory).joinpath("?"))
    except FileNotFoundError:
        pass

    web_container.add_fastqc(config.samples)
