from pathlib import Path
from zipfile import ZipFile

import psutil
import shutil

from MetagenApp import config
from MetagenApp.decorators import log_name
from MetagenApp.images import PipelineImage
from MetagenApp.data_loaders import metaphlan2_loader, bracken_loader
from MetagenApp.samplelist import Samples
from functools import reduce
import pandas as pd


@log_name("pear")
def pear_cmd(sample):
    out_path = f"pear/{sample.name}/{sample.name}"
    return f"pear -j {config.max_threads} -f {sample.fwd_path} -r {sample.rev_path} -o {out_path}"


@log_name("check_fastq")
def check_fastq_cmd(sample):
    out_file = f"/data/perl/{sample.name}/{sample.file[:-5]}trim.noamb.fq"
    return f"CheckFASTQ_gzipped.pl -m replace -i {sample.path} -o {out_file}"


@log_name("vsearch-fastq2fasta")
def fq2fa_cmd(sample):
    out_file = f"/data/vsearch/{sample.name}/{sample.file[:-13]}trim.noamb.fa"
    return f"vsearch --threads {config.max_threads} --fastq_filter {sample.path} -fastaout {out_file} --fastq_qmax 42"


@log_name("vsearch-repeatmasking")
def repeatmasking_cmd(sample):
    out_file = f"/data/vsearch/{sample.name}/{sample.file[:-13]}trim.noamb_masked.fa"
    return f"vsearch --threads {config.max_threads} --fastx_mask {sample.path} --min_unmasked_pct 50 " \
        f"--fasta_width 0 --hardmask --qmask dust --fastaout {out_file}"


def concat_final_cmd(sample):
    out_path = f"/data/perl/{sample.name}/{sample.name}"
    return f"concat_final.pl {sample.fwd_path} {sample.rev_path} {out_path}"


@log_name("bowtie2")
def human_decont1_cmd(sample):
    for s in sample.paths:
        if s.endswith("1P"):
            fwd = s
        if s.endswith("2P"):
            rev = s
        if s.endswith("1U"):
            unp1 = s
        if s.endswith("2U"):
            unp2 = s
    mm = "" if psutil.virtual_memory().available < 8 * 1024 ** 3 else "--mm "
    out = f"/data/bowtie2/{sample.name}/{sample.name}_mapped_unmapped.sam"
    return f"bowtie2 -p {config.max_cpu} {mm}-x /human_ref/GCA_000001405.15_GRCh38_no_alt_analysis_set.fna.bowtie_index " \
        f"-1 {fwd} -2 {rev} -U {unp1},{unp2} -S {out}"


@log_name("samtools-view-sort-fastq")
def human_decont2_cmd(sample):
    mem_avail = int(psutil.virtual_memory().available / 1024 ** 3)
    mpt = int(mem_avail / (int(config.max_cpu / 2)))
    mpt = f"{int(psutil.virtual_memory().available / 1024 ** 2)}M" if mpt == 0 else f"{mpt}G"
    out_fwd = f"/data/samtools/{sample.name}/{sample.name}_decont_1.fq"
    out_rev = f"/data/samtools/{sample.name}/{sample.name}_decont_2.fq"
    out_unp = f"/data/samtools/{sample.name}/{sample.name}_decont_U.fq"
    return f'samtools view -bS -f 12 -F 256 --threads {int(config.max_cpu / 2)} {sample.path} |' \
        f' samtools sort -n --threads {int(config.max_cpu / 2)} -m {mpt}G -T /tmp/ | ' \
        f'samtools fastq -0 {out_unp} -1 {out_fwd} -2 {out_rev} -'


def cat_cmd(sample):
    outfile = f"/data/bash/{sample.name}/{sample.name}_final.fa"
    paths = " ".join([p for p in sample.paths])
    return f'cat {paths} > {outfile}'


@log_name("kraken2")
def kraken2_cmd(sample):
    mm = "--memory-mapping " if psutil.virtual_memory().available < 9e+9 else ""
    out = f"/data/bracken/{sample.name}/{sample.name}_kraken.out"
    report = f"/data/bracken/{sample.name}/{sample.name}_kraken.report"
    return f"kraken2 --db /db/ --threads {config.max_cpu} {mm}--output {out} --report {report} {sample.path}"

def table2biom(filepath):
    out = f"/data/{filepath.stem}.biom"
    return f"biom convert -i /data/{filepath.name} -m /data/map.txt -o {out} --table-type='OTU table' --to-hdf5"


def biom2qza(filepath):
    return f"qiime tools import --input-path {filepath.name} --type 'FeatureTable[Frequency]' --input-format BIOMV210Format --output-path {filepath.stem}.qza"


def qiime_beta(filepath, rank):
    return f"qiime diversity beta --p-metric braycurtis --i-table {filepath.name} --o-distance-matrix distance_{rank}.qza --p-n-jobs {config.max_cpu}"


def qiime_pcoa(filepath, rank):
    return f"qiime diversity pcoa --i-distance-matrix {filepath.name} --o-pcoa pcoa_{rank}.qza"


def qiime_rel_freq(filepath, rank):
    return f"qiime feature-table relative-frequency --i-table {filepath.name} --o-relative-frequency-table abundance_rel_{rank}.qza"


def qiime_biplot(filepath, rank):
    return f"qiime diversity pcoa-biplot --i-pcoa {filepath.name} --i-features abundance_rel_{rank}.qza --o-biplot biplot_{rank}.qza --quiet"


def qiime_emperor(filepath, rank):
    return f"qiime emperor plot --i-pcoa biplot_{rank}.qza --m-metadata-file map.txt --output-dir emperorplot_{rank} --quiet"


def qiime_export_emperor(filepath, rank):
    return f"qiime tools export --input-path emperorplot_{rank}/visualization.qzv --output-path emperorplot_{rank}"


def merge_bracken_reports(samples, level, homo):
    Path(samples[0].host_dir / 'qiime').mkdir(exist_ok=True)
    dfs1 = []
    dfs2 = []
    for s in samples:
        f = s.host_dir / s.container_dir / s.file
        df1 = pd.read_csv(f, index_col=0, header=0, sep="\t", usecols=["name", "new_est_reads"])
        df2 = pd.read_csv(f, index_col=0, header=0, sep="\t", usecols=["taxonomy_id", "new_est_reads"])
        df1.columns = [s.name]
        dfs1.append(df1)
        df2.columns = [s.name]
        dfs2.append(df2)

    res = reduce(lambda left, right: left.merge(right, on="name", how="outer"), dfs1)
    res = res.fillna(0).astype(int)
    try:
        res.drop([homo])
    except KeyError:
        pass
    res[res < 5] = 0  # low abundance treshold
    res = res[(res.T != 0).any()]  # drop rows with only zeros
    fn_all = samples[0].host_dir / 'qiime' / f"abundance_all_{level}.tsv"
    res.to_csv(fn_all, sep="\t")

    top_abundant = res.sum(axis=1).sort_values(ascending=False).head(15).index
    abundance_table_top = res.loc[top_abundant]
    fn_top = samples[0].host_dir / 'qiime' / f"abundance_top_{level}.tsv"
    abundance_table_top.to_csv(fn_top, sep="\t")

    return fn_all, fn_top


@log_name("krona(kraken2)")
def krona_kraken2_cmd(sample):
    out = f"/data/krona/{sample.name}/{sample.name}_kronaplot.html"
    return f"ktImportText -o {out} {sample.path}"


@log_name("krona(metaphlan2)")
def krona_mpl_cmd(sample):
    out = f"/data/krona/{sample.name}/{sample.name}_mpl_kronaplot.html"
    return f"ktImportText -q -o {out} {sample.path}"


@log_name("metaphlan2")
def metaphlan2_cmd(sample):
    out = f"/data/metaphlan2/{sample.name}/{sample.name}_profiled_metagenome.txt"
    return f"metaphlan2.py --mpa_pkl /usr/local/bin/metaphlan_databases/mpa_v20_m200.pkl " \
        f"--bowtie2db /usr/local/bin/metaphlan_databases/ --input_type fasta  " \
        f"--nproc {config.max_threads} --no_map {sample.path} {out}"


@log_name("humann2")
def humann2_cmd(sample):
    profile = f"/data/metaphlan2/{sample.name}/{sample.name}_profiled_metagenome.txt"
    return f"humann2 --threads {config.max_cpu} --nucleotide-database /chocophlan/ --protein-database /uniref/ " \
        f"--memory-use maximum --taxonomic-profile {profile} " \
        f"--metaphlan-options='--mpa_pkl /metaphlan_db/mpa_v20_m200.pkl --bowtie2db /metaphlan_db/' " \
        f"--input {sample.path} --output-basename {sample.name} " \
        f"--output /data/humann2/{sample.name} --remove-temp-output"


def run_wgs(web_container, dockerclient):
    pear = PipelineImage(dockerclient, image="quay.io/biocontainers/pear:0.9.6--he4cf2ce_4")
    perl = PipelineImage(dockerclient, dockerfile="containers/perl")
    bash = PipelineImage(dockerclient, dockerfile="containers/bash")
    vsearch = PipelineImage(dockerclient, image="quay.io/biocontainers/vsearch:2.9.1--h96824bc_0")
    bracken = PipelineImage(dockerclient, dockerfile="containers/bracken",
                            db_host=config.kraken2_db,
                            db_container="/db/",
                            db_url="ftp://ftp.ccb.jhu.edu/pub/data/kraken2_dbs/minikraken2_v2_8GB_201904_UPDATE.tgz",
                            data_loader=bracken_loader
                            # set db name in config to filename if url is changed!!!
                            )


    krona = PipelineImage(dockerclient, image="quay.io/biocontainers/krona:2.7--pl526_2")

    bowtie2 = PipelineImage(dockerclient, image="quay.io/biocontainers/bowtie2:2.3.4.3--py36h2d50403_0",
                            db_host=config.GRCh38,
                            db_container="/human_ref/",
                            db_url="ftp://ftp.ncbi.nlm.nih.gov/genomes/archive/old_genbank/Eukaryotes/vertebrates_mammals/Homo_sapiens/GRCh38/seqs_for_alignment_pipelines/GCA_000001405.15_GRCh38_no_alt_analysis_set.fna.bowtie_index.tar.gz",
                            db_rename="GRCh38"
                            )

    samtools = PipelineImage(dockerclient, image="quay.io/biocontainers/samtools:1.9--h8ee4bcc_1")

    humann2 = PipelineImage(dockerclient, image="quay.io/biobakery/humann2:latest")
    humann2.add_database(config.humann2_chocophlan,
                         "/chocophlan/",
                         url="http://huttenhower.sph.harvard.edu/humann2_data/chocophlan/full_chocophlan.tar.gz",
                         rename="chocophlan")
    humann2.add_database(config.humann2_uniref,
                         "/uniref/",
                         url="http://huttenhower.sph.harvard.edu/humann2_data/uniprot/uniref_annotated/uniref50_annotated_1_1.tar.gz",
                         rename="uniref")
    humann2.add_database(config.metaphlan2_db,
                         "/metaphlan_db/",
                         url="https://bitbucket.org/biobakery/metaphlan2/downloads/mpa_v20_m200.tar",
                         data_loader=metaphlan2_loader)

    qiime2 = PipelineImage(dockerclient, image="qiime2/core:2019.4")

    trim_all = Samples.collect("trimmomatic", ".*_*")

    bowtie2.run(human_decont1_cmd, trim_all, 1)
    trim_all.remove_dir()
    mapped_reads = Samples.collect("bowtie2", ".*mapped_unmapped\.sam")
    samtools.run(human_decont2_cmd, mapped_reads, 1)
    mapped_reads.remove_dir()
    decont = Samples.collect("samtools", ".*_decont_[12]\.fq", pe=True)

    pear.run(pear_cmd, decont, config.max_container)
    decont.remove_dir()
    assembled_fwd = Samples.collect("pear", ".*unassembled\.forward\.fastq")
    assembled_rev = Samples.collect("pear", ".*unassembled\.reverse\.fastq")
    unassembled = Samples.collect("pear", ".*\.assembled\.fastq")

    perl.run(check_fastq_cmd, assembled_fwd, config.max_container)
    perl.run(check_fastq_cmd, assembled_rev, config.max_container)
    perl.run(check_fastq_cmd, unassembled, config.max_container)
    assembled_fwd.remove_dir()

    assbld_fwd_noamb = Samples.collect("perl", ".*unassembled\.forward\.trim\.noamb\.fq")
    assbld_rev_noamb = Samples.collect("perl", ".*unassembled\.reverse\.trim\.noamb\.fq")
    unassbld_noamb = Samples.collect("perl", ".*\.assembled\.trim\.noamb\.fq")

    vsearch.run(fq2fa_cmd, assbld_fwd_noamb, config.max_container)
    vsearch.run(fq2fa_cmd, assbld_rev_noamb, config.max_container)
    vsearch.run(fq2fa_cmd, unassbld_noamb, config.max_container)
    assbld_fwd_noamb.remove_dir()

    assbld_fwd_trim_noamb_fa = Samples.collect("vsearch", ".*unassembled\.forward\.trim\.noamb\.fa")
    assbld_rev_trim_noamb_fa = Samples.collect("vsearch", ".*unassembled\.reverse\.trim\.noamb\.fa")
    unassbld_trim_noamb_fa = Samples.collect("vsearch", ".*\.assembled\.trim\.noamb\.fa")

    vsearch.run(repeatmasking_cmd, assbld_fwd_trim_noamb_fa, config.max_container)
    vsearch.run(repeatmasking_cmd, assbld_rev_trim_noamb_fa, config.max_container)
    vsearch.run(repeatmasking_cmd, unassbld_trim_noamb_fa, config.max_container)
    assbld_fwd_trim_noamb_fa.remove_files()
    assbld_rev_trim_noamb_fa.remove_files()
    unassbld_trim_noamb_fa.remove_files()

    repeatmasked_fa = Samples.collect("vsearch", ".*unassembled.*trim\.noamb_masked\.fa", pe=True)
    perl.run(concat_final_cmd, repeatmasked_fa, config.max_container)
    repeatmasked_fa.remove_dir()
    match_fa = Samples.collect("perl", ".*\.fa")
    bash.run(cat_cmd, match_fa, 1)
    match_fa.remove_dir()

    final_fa = Samples.collect("bash", ".*_final\.fa")

    bracken.run(kraken2_cmd, final_fa, 1)
    final_fa.remove_dir()

    kreport = Samples.collect("bracken", ".*kraken\.report")

    web_container.get_metadata()

    ranks = ["S", "G", "F", "O", "C", "P"]
    homo = ["Homo sapiens", "Homo", "Hominidae", "Primates", "Mammalia", "qzaChordata"]
    for rank, h in zip(ranks, homo):
        @log_name(f"bracken-{rank}")
        def bracken_cmd(sample, rank=rank):
            out = f"/data/bracken/{sample.name}/{sample.name}_bracken_{rank}.report"
            return f"bracken -d /db/ -i {sample.path} -o {out} -r {config.readlen} -t 20 -l {rank}"

        bracken.run(bracken_cmd, kreport, config.max_container, rank=rank)
        breport = Samples.collect("bracken", f".*_bracken_{rank}\.report")
        if len(breport) >= 4:
            ab_all, ab_top = merge_bracken_reports(breport, rank, h)
            qiime2.run_single(table2biom, ab_all)
            qiime2.run_single(table2biom, ab_top)
            biom_all = ab_all.parents[0].joinpath(str(ab_all.stem) + ".biom")
            biom_top = ab_top.parents[0].joinpath(str(ab_top.stem) + ".biom")
            qiime2.run_single(biom2qza, biom_all)
            qiime2.run_single(biom2qza, biom_top)
            qza_all = ab_all.parents[0].joinpath(str(ab_all.stem) + ".qza")
            qza_top = ab_all.parents[0].joinpath(str(ab_all.stem) + ".qza")
            qiime2.run_single(qiime_beta, qza_all, rank)
            dist = ab_all.parents[0].joinpath(f"distance_{rank}.qza")
            qiime2.run_single(qiime_pcoa, dist, rank)
            pcoa = ab_all.parents[0].joinpath(f"pcoa_{rank}.qza")
            qiime2.run_single(qiime_rel_freq, qza_top, rank)
            qiime2.run_single(qiime_biplot, pcoa, rank)
            viz = ab_all.parents[0].joinpath(f"emperorplot_{rank}")
            qiime2.run_single(qiime_emperor, viz, rank)
            qiime2.run_single(qiime_export_emperor, viz, rank)
            abundance_dir = ab_all.parents[1]
            to_remove = ["*.qza", "*.qzv", "index.html"]
            for fn in to_remove:
                [path.unlink() for path in abundance_dir.glob(f'**/{fn}')]

    emperor_plots = config.input_directory.joinpath("qiime").glob("emperorplot_*")
    for src in emperor_plots:
        target = config.input_directory.joinpath(f"emperor_plots/{src.parts[-1]}")
        if target.is_dir():
            shutil.rmtree(target)
        shutil.move(src, target)

    web_container.add_taxonomy_wgs(config.samples)
    web_container.krakenreport2kronatxt(config.samples)

    krakenkrona = Samples.collect("bracken", ".*_kraken2krona.txt")
    krona.run(krona_kraken2_cmd, krakenkrona, config.max_container)
    krakenkrona.remove_files()

    web_container.add_krona(config.samples)

    web_container.add_emperor()
    tables = config.input_directory.joinpath("qiime").glob("abundance_all*")
    config.input_directory.joinpath(f"abundance_tables/").mkdir(exist_ok=True)
    [shutil.move(t, config.input_directory.joinpath(f"abundance_tables/{t.parts[-1]}")) for t in tables]
    shutil.rmtree(config.input_directory.joinpath("qiime"))

