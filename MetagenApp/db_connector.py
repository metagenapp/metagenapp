import time
import platform
import socket
from collections import namedtuple
from subprocess import check_output
from psycopg2 import OperationalError, DatabaseError

import psycopg2
import psycopg2.errors
import subprocess

from MetagenApp import config


class DbConnector:
    def __init__(self, set_ip=False):
        if set_ip:
            if platform.system() != "Linux":
                self.ip = socket.gethostbyname(socket.gethostname())
            else:
                # get db container ip:
                self.set_ip()

        else:
            self.ip = None

    def set_ip(self):
        try:
            self.ip = check_output(
                ["docker", "inspect", "-f", '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}',
                 "metagen_db"]).strip().decode()
        except:
            self.ip = None

    def exec(self, query, fetch=None):
        conn = psycopg2.connect(dbname='postgres',
                                user='postgres',
                                host=self.ip,
                                password='docker',
                                port=config.pg_port)

        c = conn.cursor()
        c.execute(query)
        conn.commit()
        if fetch in ["all", "one"]:
            try:
                rtn = c.fetchall() if fetch == "all" else c.fetchone()
            except:
                rtn = None
        elif fetch is None:
            rtn = None
        else:
            rtn = fetch(c)
        conn.close()

        return rtn

    def update_run(self, status):
        """
        Update status field in pipeline_pipelinerun
        :param id: run_id
        :param status: string for new status
        :return: None
        """
        self.exec(f"UPDATE results_pipelinerun SET status = '{status}' WHERE id = '{config.run_id}'")

    def fetch_run(self, status):
        """
        get run_with status
        :param status: string
        :return: tuple containing run relation
        """
        run = self.exec(f"SELECT * FROM results_pipelinerun WHERE status = '{status}'", fetch="one")

        return run

    def fetch_samples(self, drctry):
        """
        get all samples with same run_id
        :param run_id: integer
        :return: list of tuples containing sample relations
        """

        def cursor_func(c):
            try:
                smpls = c.fetchall()

            except AttributeError:
                smpls = []

            Sample = namedtuple("sample", ["name", "host_dir", "fwd", "rev"])
            samples = [Sample(s[0], drctry, s[1].split("/")[-1], s[2].split("/")[-1]) for s in smpls] if smpls else []

            return samples

        samples = self.exec(
            f"SELECT name, sample_file_fwd, sample_file_rev FROM  results_sample WHERE run_id = '{config.run_id}'",
            cursor_func)

        return samples

    def fetch_trim_params(self, run):
        trim = self.exec(
            f"SELECT leading_quality, trailing_quality, minimum_length, sliding_window, sliding_window_quality, "
            f"illumina_clipping FROM  pipeline_pipelinetrim WHERE run_id = '{run}'", "one")

        return trim

    def fetch_trimclip_params(self, run):
        trimclip = self.exec(
            f"SELECT seed_mismatches, palindrome_clip_theshold, simple_clipping_theshold, minimum_Adapter_Length "
            f"FROM pipeline_pipelinetrimclip WHERE run_id = '{run}'", "one")

        return trimclip

    def check_existing_run(self):
        """
        check if analysis name is already taken
        :param name: name of analysis
        :return: tuple containing run relation
        """
        connection = False
        while not connection:
            try:
                run = self.exec(f"SELECT * FROM results_pipelinerun WHERE analysis_en = '{config.analysis}'", "all")
                connection = True

                return run

            except (OperationalError, DatabaseError):
                time.sleep(0)


